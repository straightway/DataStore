# Extensible Data Threads

Data can be be organized in "threads" of data chunks, where new chunks can be appended.

## Requirements

1. Existing chunks cannot be changed any more
2. New chunks can only be appended to data threads by authorized entities.

## Algorithm

### Initialization

Create an initial new asymmetric crypto key pair, consisting of the private key _P_<sub>0</sub> and the public key _K_<sub>0</sub> and a symmetric crypto key _S_<sub>0</sub>

### Add a chunk of data

Given: A series of data chunks _D_<sub>0</sub>.._D_<sub>_n_ - 1</sub> (with 0 <= _n_) and a new chunk of data _D_<sub>_n_</sub> and the triple (_P_<sub>_n_</sub>, _K_<sub>_n_</sub>, _S_<sub>_n_</sub>), which was created for _D_<sub>n - 1</sub>, or at initialization for _n_ = 0.

1. Create another triple (_P_<sub>_n_ + 1</sub>, _K_<sub>_n_ + 1</sub>, _S_<sub>_n_ + 1</sub>) for the next chunk _D_<sub>n + 1</sub>
2. Create a "publish data structure" _C_<sub>_n_</sub>, consisting of the following fields:
    - Data: _D_<sub>n</sub>
    - NextChunkID: _K_<sub>_n_ + 1</sub>
    - NextChunkDataKeyDiff: _S_<sub>_n_</sub> XOR _S_<sub>_n_ + 1</sub>
3. Publish: SIGNED(CRYPTED(_C_<sub>_n_</sub>, _S_<sub>_n_</sub>), _P_<sub>_n_</sub>) under the ID _K_<sub>_n_<sub>
4. Discard _P_<sub>_n_</sub>

### Description ###

The basic idea is to have a chain of data chunks, where one data chunk already contains the ID of its successor. This ID is the public key of an asymmetric crypto key pair, which must be stored and later be used to create the successor chunk. Without this key pair, no new data chunks can be appended to the data thread.

The asymmetric crypto key pair is created exclusively for a chunk, and after creating and signing its "publish data structure", the private part of the key pair is discarded (i.e. not stored). This way, a data chunk is "read-only", as it is impossible to create another chunk with the same crypto key pair.

Every peer can verify whether a given data chunk is correctly created by checking its signature using the chunk ID as public key. Invalid chunks should be immediately discarded.

The contents of the chunks are encrypted using a symmetric crypto key. This key changes with each new chunk. The crypto key for a chunk can be computed from the crypto key for its predecessor XOR the "NextChunkDataKeyDiff" field of the predecessor.

### Extension ###

The "publish data structure" may contain a list of pairs fo next chunk IDs and chunk data key diffs. The asymmetric crypto key pair for each chunk ID may be distributed to other parties to allow them appending data to the thread (this way, the thread becomes a tree).