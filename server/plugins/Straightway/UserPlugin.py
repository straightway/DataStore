from Plugin import PluginManager
from .SelfSignedCert import decodeBase64

@PluginManager.registerTo("User")
class UserPlugin(object):
    def __init__(self, *args, **kwargs):
        super(UserPlugin, self).__init__(*args, **kwargs)

    def setTempCert(self, address, cert):
        siteData = self.getSiteData(address, create=False)
        currCertId = None
        if "cert" in siteData:
            currCertId = siteData["cert"]
        certSaveKey = "savedCert_" + address
        if certSaveKey in siteData:
            self.setCert(address, siteData[certSaveKey])
            if currCertId and currCertId in self.certs:
                self.deleteCert(currCertId)
            del siteData[certSaveKey]
        if cert:
            newCert = decodeBase64(cert)
            siteData[certSaveKey] = currCertId
            authAddress = newCert["auth_address"]
            self.certs[newCert["auth_address"]] = newCert
            self.save()
            self.setCert(address, authAddress)
