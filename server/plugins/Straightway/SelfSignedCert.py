from Crypt import CryptBitcoin

import base64
import json


def create(**baseCert):
    cert = _createSelfSignedCertificate(baseCert)
    return encodeBase64(cert)


def _createSelfSignedCertificate(cert):
    cert["auth_privatekey"] = CryptBitcoin.newPrivatekey()
    cert["auth_address"] = CryptBitcoin.privatekeyToAddress(cert["auth_privatekey"])
    _setDefaultIfMissing(cert, "auth_user_name", cert["auth_address"])
    _setDefaultIfMissing(cert, "auth_type", "web")
    _selfSign(cert)
    return cert


def _setDefaultIfMissing(cert, item, defaultValue):
    if _isMissing(cert, item):
        cert[item] = defaultValue


def _isMissing(cert, item):
    return item not in cert or cert[item] is None


def _selfSign(cert):
    toSign = cert["auth_address"] + "#" + cert["auth_type"] + "/" + cert["auth_user_name"]
    cert["cert_sign"] = CryptBitcoin.sign(toSign, cert["auth_privatekey"])


def encodeBase64(cert):
    jsonEncoded = json.dumps(cert)
    jsonEncodedArray = jsonEncoded.encode("ascii")
    return base64.b64encode(jsonEncodedArray).decode("ascii")

def decodeBase64(base64Str):
    base64Decoded = base64.b64decode(base64Str)
    return json.loads(base64Decoded)
