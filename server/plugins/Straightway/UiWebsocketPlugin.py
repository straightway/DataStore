import shutil

from Plugin import PluginManager
from . import SelfSignedCert
from pathlib import Path
from os.path import relpath, isdir, exists

@PluginManager.registerTo("UiWebsocket")
class UiWebsocketPlugin(object):
    # noinspection PyMethodMayBeStatic
    def actionCreateSelfSignedIdentity(self, _):
        return SelfSignedCert.create(auth_type="web")

    def actionSetTempCert(self, _, siteAddress, cert):
        self.user.setTempCert(siteAddress, cert)
        return "ok"

    def actionGetCert(self, _, address):
        return self.user.getCert(address)

    def actionDeleteLocally(self, _, innerPath):
        path = self.site.storage.getPath(innerPath)
        basePath = self.site.storage.directory
        if not exists(path):
            return "ok"

        isDir = isdir(path)
        if isDir:
            files = [relpath(p, basePath) for p in Path(path).rglob("*") if not isdir(p)]
        else:
            files = [innerPath]

        print("Deleting " + str(files))

        for file in files:
            self.site.storage.delete(file)

        if isDir:
            shutil.rmtree(path)

        return "ok"