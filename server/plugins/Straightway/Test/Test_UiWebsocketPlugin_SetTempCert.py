from Straightway import SelfSignedCert

import base64
import pytest
import json

_certBase64 = SelfSignedCert.create()
_cert = json.loads(base64.b64decode(_certBase64))

@pytest.mark.usefixtures("resetSettings")
class Test_UiWebsocketPlugin_SetTempCert:
    def test_setCertificate(self, ui_websocket, site, user):
        assert ui_websocket.testAction("SetTempCert", site.address, _certBase64) == "ok"
        siteCert = user.getCert(site.address)
        assert siteCert == _cert