from Straightway import SelfSignedCert

import base64
import pytest
import json

_certBase64 = SelfSignedCert.create()
_cert = json.loads(base64.b64decode(_certBase64))

@pytest.mark.usefixtures("resetSettings")
class Test_UserPlugin_setAnonymousMode:
    def test_setCertificate(self, user, site):
        user.setTempCert(site.address, _certBase64)
        assert user.getCert(site.address) == _cert
        user.setTempCert(site.address, None)
        assert user.getCert(site.address) == None

    def test_resetCertificate(self, user, site):
        defaultCert = json.loads(base64.b64decode(SelfSignedCert.create()))
        user.certs["self.signed"] = defaultCert
        user.setCert(site.address, "self.signed")
        currentCert = user.getCert(site.address)
        assert currentCert == defaultCert
        user.setTempCert(site.address, _certBase64)
        assert user.getCert(site.address) == _cert
        user.setTempCert(site.address, None)
        assert user.getCert(site.address) == defaultCert
        del user.certs["self.signed"]
