import base64
import json

from Crypt import CryptBitcoin
from Straightway import SelfSignedCert


class Test_SelfSignedCert:
    def test_certificate_contains_privateKey(self):
        cert = self._callCreateSelfSignedIdentity()
        assert CryptBitcoin.privatekeyToAddress(cert["auth_privatekey"]) == cert["auth_address"]

    def test_certificateAuthType_defaultIs_web(self):
        cert = self._callCreateSelfSignedIdentity()
        assert cert["auth_type"] == "web"

    def test_certificateAuthType_None_usesWeb(self):
        cert = self._callCreateSelfSignedIdentity(auth_type=None)
        assert cert["auth_type"] == "web"

    def test_certificateAuthType_isUsedIfSpecified(self):
        cert = self._callCreateSelfSignedIdentity(auth_type="lalala")
        assert cert["auth_type"] == "lalala"

    def test_specifiedUserName_isUsedInCertificate(self):
        cert = self._callCreateSelfSignedIdentity(auth_user_name="userName")
        assert cert["auth_user_name"] == "userName"

    def test_withoutUserName_usesAddress(self):
        cert = self._callCreateSelfSignedIdentity()
        assert cert["auth_user_name"] == cert["auth_address"]

    def test_withNoneUserName_usesAddress(self):
        cert = self._callCreateSelfSignedIdentity(auth_user_name=None)
        assert cert["auth_user_name"] == cert["auth_address"]

    def test_createsSelfSignedCertificate(self):
        cert = self._callCreateSelfSignedIdentity(auth_user_name="userName")
        toSign = cert["auth_address"] + "#" + cert["auth_type"] + "/userName"
        assert CryptBitcoin.verify(toSign, cert["auth_address"], cert["cert_sign"])

    @staticmethod
    def _callCreateSelfSignedIdentity(**kwargs):
        result = SelfSignedCert.create(**kwargs)
        base64_decoded = base64.b64decode(result)
        return json.loads(base64_decoded)
