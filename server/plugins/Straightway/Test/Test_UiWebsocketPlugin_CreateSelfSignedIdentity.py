import base64
import pytest
import json

@pytest.mark.usefixtures("resetSettings")
class Test_UiWebsocketPlugin_CreateSelfSignedIdentity:
    def test_certificate_isComplete(self, ui_websocket):
        result = ui_websocket.testAction("CreateSelfSignedIdentity", userName = "userName")
        base64_decoded = base64.b64decode(result)
        cert = json.loads(base64_decoded)
        assert cert["auth_privatekey"] != ""
        assert cert["auth_type"] == "web"
        assert cert["auth_address"] != ""
        assert cert["auth_user_name"] == "userName"
        assert cert["cert_sign"] != ""

    def test_withoutUserName_usesAuthAddress(self, ui_websocket):
        result = ui_websocket.testAction("CreateSelfSignedIdentity")
        base64_decoded = base64.b64decode(result)
        cert = json.loads(base64_decoded)
        assert cert["auth_user_name"] == cert["auth_address"]
