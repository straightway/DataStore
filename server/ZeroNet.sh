#!/bin/bash

SCRIPT_DIR=`realpath $( cd ${0%/*} && pwd -P )`
TOR_PORT=9050

function createInstanceDirs() {
  echo "Script directory: $SCRIPT_DIR"
  mkdir -p "$SCRIPT_DIR/data"
  mkdir -p "$SCRIPT_DIR/log"
}

function zeroNetStarter() {
 if [ x$DISPLAY != x ] || [[ "$OSTYPE" == "darwin"* ]]
  then
      # Has gui, open browser
      echo "$SCRIPT_DIR/start.py"
  else
      # No gui
      echo "$SCRIPT_DIR/zeronet.py"
  fi
}

function runZeroNet() {
  cd "$SCRIPT_DIR"
  runtime/bin/python3 `zeroNetStarter` --dist_type bundle_linux64 --data_dir "$SCRIPT_DIR/data" --log_dir "$SCRIPT_DIR/log" --config_file $SCRIPT_DIR/zeronet.conf "$@" >"$SCRIPT_DIR/log/ZeroNet.out" 2>&1 &
  ZERONET_PID=$!
  echo "ZeroNet PID: $ZERONET_PID"
  wait $ZERONET_PID
}

function torPid() {
  lsof -t -i:$TOR_PORT -sTCP:LISTEN
}

function startTor() {
  if [ -z `torPid` ]
  then
    tor --runasdaemon 1 -f $SCRIPT_DIR/torrc 2>&1 > "$SCRIPT_DIR/log/TOR.out"
    echo "TOR started, PID `torPid`"
  else
    echo "TOR already running, PID `torPid`"
  fi
}

function numberOfTorUsers() {
  lsof -t -i:$TOR_PORT | grep -v `torPid` | wc -l
}

function termThenKill() {
  kill $1
  local __NUMBER_OF_TRIES=10
  while kill -0 $1 2>/dev/null; do
    __NUMBER_OF_TRIES=$(($__NUMBER_OF_TRIES - 1))
    if [ $__NUMBER_OF_TRIES -lt 8 ]
    then
      echo "waiting ... ($__NUMBER_OF_TRIES)"
    fi
    
    sleep 1
    if [ $__NUMBER_OF_TRIES -eq 0 ]
    then
      echo "Killing $1"
      kill -9 $1
      break
    fi
  done
}

function shutDownTOR() {
  local __TOR_PID=`torPid`
  if [ -z "$__TOR_PID" ]
  then
    echo "TOR is not running"
  else
    local NUMBER_OF_ZERONET_INSTANCES=`numberOfTorUsers`
    if [ $NUMBER_OF_ZERONET_INSTANCES -eq 0 ]
    then
      echo "Shutting down TOR $__TOR_PID"
      termThenKill $__TOR_PID
      echo "Done."
    else
      echo "Don't shut down TOR as $NUMBER_OF_ZERONET_INSTANCES ZeroNet instances are still running"
    fi
  fi
}

function shutDown() {
  echo
  echo "Shutting down ZeroNet $ZERONET_PID"
  termThenKill $ZERONET_PID
  echo "Done."
}

trap shutDown INT TERM

createInstanceDirs
startTor
runZeroNet
shutDownTOR

echo "Finished."
