/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.testing.bdd

import kotlin.test.fail

/**
 * Continuation of a given/when/then clause. @see Given
 */
class GivenWhen<TGiven, TResult>(
    val given: TGiven,
    val result: WhenResult<TResult>
) {
    @Suppress("TooGenericExceptionCaught")
    infix fun then(op: TGiven.(WhenResult<TResult>) -> Unit) {
        try {
            given.op(result)
            throwIgnoredResultExceptionIfExceptionWasIgnored()
        } catch (e: Throwable) {
            val ignoredException = if (result.hasIgnoredException) result.exception else null
            if (ignoredException == null) throw AssertionError(e) else throw ignoredException
        }
    }

    private fun throwIgnoredResultExceptionIfExceptionWasIgnored() {
        if (result.hasIgnoredException)
            throw result.exception as Throwable
    }
}