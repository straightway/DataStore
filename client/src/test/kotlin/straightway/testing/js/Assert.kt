package straightway.testing.js

import kotlin.test.fail
import kotlin.test.assertEquals

fun assertDoesNotThrow(block: () -> Unit) {
    try {
        block()
    } catch (x: Throwable) {
        fail("Unexpected exception: ${x::class.simpleName} (${x.message})")
    }
}

inline fun <reified TException : Throwable>assertThrows(vararg expectedMessageLines: String, block: () -> Unit) {
    try {
        block()
    } catch(x: Throwable) {
        if (x !is TException) {
            fail("Invalid exception type. Expected: ${TException::class.simpleName}, got: ${x::class.simpleName}")
        }

        x.checkMessage(expectedMessageLines)
        return
    }

    fail("No exception occurred, expected: ${TException::class.simpleName}")
}

fun <T> assertEquals(a: Array<out T>, b: Array<out T>, message: String? = null) =
        assertEquals(a.toList(), b.toList(), message)

fun assertFailure(vararg expectedMessageLines: String, block: () -> Unit) =
        assertThrows<Throwable>(*expectedMessageLines) { block() }

fun Throwable.checkMessage(expectedMessageLines: Array<out String>) {
    if (expectedMessageLines.isNotEmpty() && message?.matchMessageLines(expectedMessageLines) != true)
        fail("Invalid error message: \"$message\" " +
                "(expected: \"${expectedMessageLines.joinToString("\n")}\")")
}

private fun String.matchMessageLines(expectedMessageLines: Array<out String>) =
        expectedMessageLines.map { Regex(it) }.sequentiallyMatches(split("\n"))

private fun Collection<Regex>.sequentiallyMatches(lines: Collection<String>): Boolean {
    if (isEmpty() && lines.isEmpty()) return true
    if (isEmpty() || lines.isEmpty()) return false
    return first().matches(lines.first()) && drop(1).sequentiallyMatches(lines.drop(1))
}