package straightway.testing.js

import kotlin.reflect.KFunction

data class MockCall(
        val funcName: String,
        private val _params: List<dynamic>,
        val result: dynamic,
        val sequenceNumber: Int
) {
    constructor(func: KFunction<*>,
                params: List<dynamic>,
                result: dynamic,
                sequenceNumber: Int
    ) : this(func.name, params, result, sequenceNumber)

    val params: List<dynamic> get() = _params.map { if (it is ArgWithString) it.arg else it }

    fun isCallTo(expectedFunc: String, vararg expectedParams: Any?): Any {
        if (expectedFunc != funcName)
            return "Invalid function: $funcName (expected: $expectedFunc)"
        return paraterCheckResult(expectedParams)
    }

    override fun toString()=
            "$sequenceNumber: ${funcName}(" +
            "${_params.map { JSON.stringify(it) }.joinToString(", ")})" +
                    if (result is Unit) "" else " = ${JSON.stringify(result)}"

    private fun paraterCheckResult(expectedParams: Array<out Any?>): Any {
        if (expectedParams.size != _params.size)
            return "Invalid parameters (size): $_params"
        expectedParams.forEachIndexed { i, expectedParam ->
            if (!expectedParam.matches(params[i])) return "Invalid parameters ($i): $_params"
        }
        return true
    }

    private fun Any?.matches(other: Any?) =
            if (this is MockConstraint) this.matches(other)
            else JSON.stringify(this) == JSON.stringify(other)
}