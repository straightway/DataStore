package straightway.testing.js.test

interface MockedInterface {
    fun func(a: Int): String
    fun action(a: Double)
}