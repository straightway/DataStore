package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.*
import kotlin.test.Test
import kotlin.test.assertEquals

class MockCallTest {

    private val test = Given {
        MockCall(MockedInterface::func, listOf(83), "Hello World", -1)
    }

    @Test
    fun isCallTo_withSameFunction_and_Parameters_yieldsTrue() =
            test when_ {
                isCallTo("func", 83)
            } then {
                assertEquals(true, it.result)
            }

    @Test
    fun isCallTo_withDifferentFunction_yieldsErrorMessage() =
            test when_ {
                isCallTo("action", 83)
            } then {
                assertEquals(
                        "Invalid function: ${MockedInterface::func.name} " +
                        "(expected: action)",
                        it.result)
            }

    @Test
    fun isCallTo_wrongNumberOfParameters_yieldsErrorMessage() =
            test when_ {
                isCallTo("func", 1, 2)
            } then {
                assertEquals("Invalid parameters (size): ${listOf(83)}", it.result)
            }

    @Test
    fun isCallTo_wrongNumberParameterValues_yieldsErrorMessage() =
            test when_ {
                isCallTo("func", 1)
            } then {
                assertEquals("Invalid parameters (0): ${listOf(83)}", it.result)
            }

    @Test
    fun isCallTo_withPassingMockConstraint_isSuccess() =
            test when_ {
                isCallTo("func", any)
            } then {
                assertEquals(true, it.result)
            }

    @Test
    fun isCallto_withArgWithString_checksArg() =
            Given {
                MockCall(MockedInterface::func, listOf(ArgWithString(83, "ET")), "Hello World", -1)
            } when_ {
                isCallTo("func", 83)
            } then {
                assertEquals(true, it.result)
            }

    @Test
    fun isCallto_withArgWithString_passesArgToPredicate() =
            Given {
                MockCall(MockedInterface::func, listOf(ArgWithString(83, "ET")), "Hello World", -1)
            } when_ {
                isCallTo("func", itIs { it == 83 })
            } then {
                assertEquals(true, it.result)
            }

    @Test
    fun call_toString_yieldsProperStringRepresentation() =
            Given {
                MockCall(MockedInterface::func, listOf("a", null, 83), "Result", 13)
            } when_ {
                toString()
            } then {
                assertEquals("13: func(\"a\", null, 83) = \"Result\"", it.result)
            }

    @Test
    fun call_toString_withoutReturnValue_yieldsProperStringRepresentation() =
            Given {
                MockCall(MockedInterface::action, listOf("a", null, 83), null, 13)
            } when_ {
                toString()
            } then {
                assertEquals("13: action(\"a\", null, 83) = null", it.result)
            }

    @Test
    fun params_yieldsValuesOf_ArgWithString_parameters() =
            Given {
                MockCall(MockedInterface::func, listOf(ArgWithString(83, "ET")), "Hello World", -1)
            } when_ {
                params.single()
            } then {
                assertEquals(83, it.result)
            }
}