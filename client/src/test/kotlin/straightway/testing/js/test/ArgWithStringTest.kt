package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.ArgWithString
import kotlin.test.Test
import kotlin.test.assertEquals

class ArgWithStringTest {

    private val test = Given {
        ArgWithString(83, "EightyThree")
    }

    @Test
    fun arg_isAccessible() =
            test when_ {
                arg
            } then {
                assertEquals(83, it.result)
            }

    @Test
    fun string_isAccessible() =
            test when_ {
                string
            } then {
                assertEquals("EightyThree", it.result)
            }

    @Test
    fun toString_returnsString() =
            test when_ {
                toString()
            } then {
                assertEquals("EightyThree", it.result)
            }
}