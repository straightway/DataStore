package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.*
import kotlin.test.Test

interface TestInterface
{
    fun fun1()
    fun fun2(p: Any)
    var writeProp: Any
    val readProp: Any
}

class AssertCallSequenceTest : TestBase() {

    private val test get() = Given {
        createTestInterfaceMock()
    }

    @Test
    fun noCalls_success() =
            test when_ {} then { verifySequence {} }

    @Test
    fun singleCall_notCalled_failure() =
            test when_ {
                this
            } then {
                assertFailure {
                    verifySequence {
                        this@then {
                            (TestInterface::fun1)()
                        }
                    }
                }
            }

    @Test
    fun singleCall_called_success() =
            test when_ {
                obj.fun1()
            } then {
                verifySequence {
                    this@then {
                        (TestInterface::fun1)()
                    }
                }
            }

    @Test
    fun singleCall_withFunctionName_called_success() =
            test when_ {
                obj.fun1()
            } then {
                verifySequence {
                    this@then {
                        "fun1"()
                    }
                }
            }

    @Test
    fun twoCalls_calledInProperOrder_success() =
            test when_ {
                obj.fun1()
                obj.fun2("Lalala")
            } then {
                verifySequence {
                    this@then {
                        (TestInterface::fun1)()
                        (TestInterface::fun2)("Lalala")
                    }
                }
            }

    @Test
    fun twoCalls_calledInWrongOrder_failure() =
            test when_ {
                obj.fun2("Lalala")
                obj.fun1()
            } then {
                assertFailure {
                    verifySequence {
                        this@then {
                            (TestInterface::fun1)()
                            (TestInterface::fun2)("Lalala")
                        }
                    }
                }
            }

    @Test
    fun singleCall_assertedTwice_fails() =
            test when_ {
                obj.fun1()
            } then {
                assertFailure {
                    verifySequence {
                        this@then {
                            (TestInterface::fun1)()
                            (TestInterface::fun1)()
                        }
                    }
                }
            }

    @Test
    fun twoCalls_calledInProperOrder_firstCallFollowingAgain_success() =
            test when_ {
                obj.fun1()
                obj.fun2("Lalala")
                obj.fun1()
            } then {
                verifySequence {
                    this@then {
                        (TestInterface::fun1)()
                        (TestInterface::fun2)("Lalala")
                    }
                }
            }

    @Test
    fun callsForDifferentMocks_sameFunction_success() {
        val otherMock = createTestInterfaceMock()
        test when_ {
            obj.fun1()
            obj.fun1()
            otherMock.obj.fun1()
        } then {
            verifySequence {
                this@then {
                    (TestInterface::fun1)()
                    (TestInterface::fun1)()
                }
                otherMock {
                    (TestInterface::fun1)()
                }
            }
        }
    }

    @Test
    fun callsForDifferentMocks_sameFunction_failure() {
        val otherMock = createTestInterfaceMock()
        test when_ {
            obj.fun1()
            obj.fun1()
            otherMock.obj.fun1()
        } then {
            assertFailure {
                verifySequence {
                    this@then {
                        (TestInterface::fun1)()
                    }
                    otherMock {
                        (TestInterface::fun1)()
                        (TestInterface::fun1)()
                    }
                }
            }
        }
    }

    private fun createTestInterfaceMock(): Mock<TestInterface> {
        return Mock {
            object : TestInterface {
                override fun fun1() = call(TestInterface::fun1)
                override fun fun2(p: Any) = call(TestInterface::fun2, p)
                override var writeProp: Any
                    get() = call(TestInterface::writeProp::get)
                    set(value) {
                        call(TestInterface::writeProp::set, value)
                    }
                override val readProp: Any
                    get() = call(TestInterface::readProp::get)
            }
        }
    }
}