package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.*
import kotlin.test.Test

class MockAssertTest {

    private val test = Given {
        Mock<MockedInterface> {
            object : MockedInterface {
                override fun func(a: Int): String = call(MockedInterface::func, a) { "Hello World" }
                override fun action(a: Double) = call(MockedInterface::action, a)
            }
        }
    }

    @Test
    fun assert_matchedCalledFunction_isSuccess() =
            test when_ {
                obj.func(83)
            } then {
                verify {
                    "func"(83) wasCalled atLeastOnce
                }
            }

    @Test
    fun assert_missingCalledFunction_isFailure() =
            test when_ {
            } then {
                assertFailure("Could not find calls for func\\(\\{\\}\\)") {
                    verify {
                        "func"(any) wasCalled atLeastOnce
                    }
                }
            }

    @Test
    fun assert_failureListsCalls() =
            test when_ {
                obj.func(83)
                obj.func(91)
            } then {
                assertFailure(
                        "Could not find calls for not existing function\\(\\)",
                        "  .*: func\\(83\\) = \"Hello World\".*",
                        "  .*: func\\(91\\) = \"Hello World\".*"
                ) {
                    verify {
                        "not existing function"() wasCalled atLeastOnce
                    }
                }
            }

    @Test
    fun assertNever_noCalls_success() =
            test when_ {
            } then {
                verify {
                    "func"(any) wasCalled never
                }
            }

    @Test
    fun assertNever_singleCall_failure() =
            test when_ {
                obj.func(83)
            } then {
                assertFailure(
                        "Expected 0 calls, additionally found:",
                        "  .*: func\\(83\\) = \"Hello World\""
                ) {
                    verify {
                        "func"(83) wasCalled never
                    }
                }
            }

    @Test
    fun assertNever_singleCall_failure_listsOnlyMatchingCalls() =
            test when_ {
                obj.func(83)
                obj.func(91)
            } then {
                assertFailure(
                        "Expected 0 calls, additionally found:",
                        "  .*: func\\(83\\) = \"Hello World\""
                ) {
                    verify {
                        "func"(83) wasCalled never
                    }
                }
            }

    @Test
    fun assertTimes1_noCalls_failure() =
            test when_ {
            } then {
                assertFailure {
                    verify {
                        "func"(any) wasCalled 1.times
                    }
                }
            }

    @Test
    fun assertTimes1_singleCall_success() =
            test when_ {
                obj.func(83)
            } then {
                verify {
                    "func"(any) wasCalled 1.times
                }
            }

    @Test
    fun assertTimes2_singleCall_failsWithProperMessage() =
            test when_ {
                obj.func(83)
            } then {
                assertFailure(
                        "Could not find calls for func\\(83\\)",
                        "  .*: func\\(83\\) = \"Hello World\".*"
                ) {
                    verify {
                        "func"(83) wasCalled 2.times
                    }
                }
            }
}