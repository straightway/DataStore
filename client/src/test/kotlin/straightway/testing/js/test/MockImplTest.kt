package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.*
import kotlin.test.*

class MockImplTest : TestBase() {

    private val test = Given {
        createSut()
    }

    @Test
    fun functionCall_returnsSetupReturnValue() =
            test when_ {
                obj.func(83)
            } then {
                assertEquals("Hello World", it.result)
            }

    @Test
    fun functionCall_isListed_withFunction() =
            test when_ {
                obj.func(83)
            } then {
                assertTrue("has proper function") {
                    calls.single().funcName == "func"
                }
            }

    @Test
    fun functionCall_isListed_withParameters() =
            test when_ {
                obj.func(83)
            } then {
                assertTrue("has proper parameters") {
                    calls.single().params == listOf(83)
                }
            }

    @Test
    fun functionCall_isListed_withReturnValue() =
            test when_ {
                obj.func(83)
            } then {
                assertTrue("has proper return value") {
                    calls.single().result == "Hello World"
                }
            }

    @Test
    fun functionCall_addsCountedUpSequenceNumbers() =
            test when_ {
                obj.func(83)
                obj.func(91)
            } then {
                val sequenceNumbers = calls.map { it.sequenceNumber }
                assertTrue(sequenceNumbers.all { 0 <= it }, "Non-negative sequence numbers")
                assertEquals(sequenceNumbers.sorted(), sequenceNumbers, "Sequence numbers in proper order")
                assertEquals(sequenceNumbers.distinct(), sequenceNumbers, "Sequence numbers are unique")
            }

    @Test
    fun functionCall_countsSequenceNumbersGlobally() =
            Given {
                object {
                    val mock1 = createSut()
                    val mock2 = createSut()
                }
            } when_ {
                mock1.obj.func(83)
                mock2.obj.func(91)
            } then {
                val sequenceNumbers = (mock1.calls + mock2.calls).map { it.sequenceNumber }
                assertEquals(sequenceNumbers.distinct(), sequenceNumbers, "Sequence numbers are unique")
            }

    @Test
    fun actionCall_isListed_withFunction() =
            test when_ {
                obj.action(83.0)
            } then {
                assertTrue("has proper function") {
                    calls.single().funcName == "action"
                }
            }

    @Test
    fun actionCall_isListed_withParameters() =
            test when_ {
                obj.action(83.0)
            } then {
                assertTrue("has proper parameters") {
                    calls.single().params == listOf(83.0)
                }
            }

    @Test
    fun actionCall_isListed_withNoReturnValue() =
            test when_ {
                obj.action(83.0)
            } then {
                assertEquals(
                        calls.single().result,
                        Unit,
                        "has null return value, actual: ${calls.single()}")
            }

    @Test
    fun actionCall_addsCountedUpSequenceNumbers() =
            test when_ {
                obj.action(83.0)
                obj.action(91.0)
            } then {
                val sequenceNumbers = calls.map { it.sequenceNumber }
                assertTrue(sequenceNumbers.all { 0 <= it }, "Non-negative sequence numbers")
                assertEquals(sequenceNumbers.sorted(), sequenceNumbers, "Sequence numbers in proper order")
                assertEquals(sequenceNumbers.distinct(), sequenceNumbers, "Sequence numbers are unique")
            }

    @Test
    fun actionCall_countsSequenceNumbersGlobally() =
            Given {
                object {
                    val mock1 = createSut()
                    val mock2 = createSut()
                }
            } when_ {
                mock1.obj.action(83.0)
                mock2.obj.action(91.0)
            } then {
                val sequenceNumbers = (mock1.calls + mock2.calls).map { it.sequenceNumber }
                assertEquals(sequenceNumbers.distinct(), sequenceNumbers, "Sequence numbers are unique")
            }

    @Test
    fun name_canBesSetInSetup() =
            test when_ {
                name
            } then {
                assertEquals("SUT", it.result)
            }

    @Test
    fun toString_yieldsProperStringRepresentations() =
            test when_ {
                toString()
            } then {
                assertEquals("Mock<MockedInterface>(SUT)", it.result)
            }

    @Test
    fun obj_toString_yieldsProperStringRepresentations() =
            test when_ {
                obj.toString()
            } then {
                assertEquals(toString(), it.result)
            }

    @Test
    fun toString_forUnnamedObject_usesHashCodeAsname() =
            Given {
                Mock<MockedInterface> {
                    object : MockedInterface {
                        override fun func(a: Int): String =
                                call(MockedInterface::func, a) { "Hello World" }
                        override fun action(a: Double) =
                                call(MockedInterface::action, a)
                    }
                }
            } when_ {
                toString()
            } then {
                assertEquals("Mock<MockedInterface>(${hashCode()})", it.result)
            }

    @Test
    fun reset_erasesCalls() =
            test while_ {
                obj.action(3.14)
            } when_ {
                reset()
            } then {
                assertTrue(calls.isEmpty())
            }

    @Test
    fun explicitSetup_callsPassedAction_onFunctionInvocation() {
        var called = false
        test while_ {
            setup { "func"(any) { called = true } }
        } when_ {
            obj.func(83)
        } then {
            assertTrue(called)
        }
    }

    @Test
    fun explicitSetup_overridesPreviousAction() {
        var called = 0
        test while_ {
            setup{
                "func"(any) { called = 1 }
                "func"(any) { called = 2 }
            }
        } when_ {
            obj.func(83)
        } then {
            assertEquals(2, called)
        }
    }

    @Test
    fun explicitSetup_passesCallParametersToAction() {
        var passedParameters: Array<out Any?> = arrayOf()
        test while_ {
            setup {
                "func"(any) { passedParameters = it }
            }
        } when_ {
            obj.func(83)
        } then {
            assertEquals(arrayOf(83), passedParameters)
        }
    }

    @Test
    fun explicitSetup_overridesReturnValue() {
        test while_ {
            setup {
                "func"(any) { "overridden result" }
            }
        } when_ {
            obj.func(83)
        } then {
            assertEquals("overridden result", it.result)
        }
    }

    @Test
    fun verify1() {
        test when_ {
            obj.func(12)
        } then {
            verify {
                "func"(12) wasCalled once
            }
        }
    }

    @Test
    fun verify2() {
        test when_ {
            obj.func(12)
        } then {
            assertFails {
                verify {
                    "func"(12) wasCalled never
                }
            }
        }
    }

    @Test
    fun verify3() {
        test when_ {
            obj.func(12)
            obj.func(14)
        } then {
            verifySequence{
                this@then {
                    "func"(12)
                    "func"(any)
                }
            }
        }
    }

    @Test
    fun verify4() {
        test when_ {
            obj.func(12)
        } then {
            assertFails {
                verifySequence {
                    this@then {
                        "func"(12)
                        "func"(any)
                    }
                }
            }
        }
    }

    private fun createSut(): Mock<MockedInterface> {
        return Mock("SUT") {
            object : MockedInterface {
                override fun func(a: Int): String = call(MockedInterface::func, a) { "Hello World" }
                override fun action(a: Double) = call(MockedInterface::action, a)
                override fun toString() = toStringResult
            }
        }
    }
}