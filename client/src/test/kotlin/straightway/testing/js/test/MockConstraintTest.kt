package straightway.testing.js.test

import straightway.testing.bdd.Given
import straightway.testing.js.any
import straightway.testing.js.itIs
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class MockConstraintTest {

    @Test
    fun _any_alwaysIsTrue() =
            Given {
                any
            } when_ {
                matches(null)
            } then {
                assertTrue(it.result)
            }

    @Test
    fun itIs_nonGeneric_withWrongType_isFalse() =
            Given {
                itIs(String::class)
            } when_ {
                matches(83)
            } then {
                assertFalse(it.result)
            }

    @Test
    fun itIs_nonGeneric_withCorrectType_isTrue() =
            Given {
                itIs(String::class)
            } when_ {
                matches("Hello")
            } then {
                assertTrue(it.result)
            }

    @Test
    fun itIs_nonGeneric_withCorrectType_failingConstraint_isFalse() =
            Given {
                itIs(String::class) { false }
            } when_ {
                matches("Hello")
            } then {
                assertFalse(it.result)
            }

    @Test
    fun itIs_generic_withWrongType_isFalse() =
            Given {
                itIs<String>()
            } when_ {
                matches(83)
            } then {
                assertFalse(it.result)
            }

    @Test
    fun itIs_generic_withCorrectType_isTrue() =
            Given {
                itIs<String>()
            } when_ {
                matches("Hello")
            } then {
                assertTrue(it.result)
            }

    @Test
    fun itIs_generic_withCorrectType_failingConstraint_isFalse() =
            Given {
                itIs<String> { false }
            } when_ {
                matches("Hello")
            } then {
                assertFalse(it.result)
            }

    @Test
    fun itIs_withoutType_withPredicateTrue_isTrue() =
            Given {
                itIs { true }
            } when_ {
                matches("Hello")
            } then {
                assertTrue(it.result, toString())
            }

    @Test
    fun itIs_withoutType_withPredicateFalse_isFalse() =
            Given {
                itIs { false }
            } when_ {
                matches("Hello")
            } then {
                assertFalse(it.result)
            }

    @Test
    fun toString_ContainsExplanationAndType_ifExplanation_isNotNull() =
            Given {
                itIs { explanation = "Hello"; true }
            } when_ {
                matches("World")
            } then {
                assertEquals("Hello: Any", toString())
            }

    @Test
    fun toString_ContainsOnlyType_ifExplanation_isNull() =
            Given {
                itIs { explanation = null; true }
            } when_ {
                matches("World")
            } then {
                assertEquals("Any", toString())
            }

    @Test
    fun toString_ContainsOnlyType_ifExplanation_isNotSet() =
            Given {
                itIs { true }
            } when_ {
                matches("World")
            } then {
                assertEquals("Any", toString())
            }
}