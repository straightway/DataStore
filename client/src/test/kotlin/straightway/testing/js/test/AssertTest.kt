package straightway.testing.js.test

import straightway.error.Panic
import straightway.testing.js.assertDoesNotThrow
import straightway.testing.js.assertFailure
import straightway.testing.js.assertThrows
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.fail

@Suppress("unused")
class AssertTest {

    inner class DoesNotThrow {
        @Test
        fun exception_causesFailure() {
            try {
                assertDoesNotThrow { throw Panic("Error") }
            } catch (x : AssertionError) {
                assertEquals("Unexpected exception: Panic (Error)", x.message)
                return
            }

            fail("Expected failure, but it did not occur")
        }

        @Test
        fun assertionFailure_causesFailure() {
            try {
                assertDoesNotThrow { fail("assertion failed") }
            } catch (x : AssertionError) {
                assertEquals("Unexpected exception: AssertionError (assertion failed)", x.message)
                return
            }

            fail("Expected failure, but it did not occur")
        }

        @Test
        fun noException_NoFailure() {
            assertDoesNotThrow {}
        }
    }

    inner class AssertThrows {
        @Test
        fun exception_isSuccess() {
            assertThrows<Panic> {
                throw Panic("Error")
            }
        }

        @Test
        fun assertionFailure_isSuccess() {
            assertThrows<AssertionError> {
                fail("Error")
            }
        }

        @Test
        fun nothingThrown_isError() {
            try {
                assertThrows<Panic> {}
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun wrongException_isError() {
            try {
                assertThrows<Panic> { throw RuntimeException("failure") }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun wrongMessage_isError() {
            try {
                assertThrows<Panic>("expected message") { throw Panic("other message") }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun singleLineMessages_matchesRegex() {
            assertThrows<Panic>(".*message.*") { throw Panic("other message") }
        }

        @Test
        fun multiLineMessages_matchesMultipleRegex() {
            assertThrows<Panic>(".*oth.*", ".*mess.*") { throw Panic("other\nmessage") }
        }

        @Test
        fun nullMessage_doesNotMatchRegex() {
            try {
                assertThrows<RuntimeException>(".*") { throw RuntimeException() }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun moreLinesThanRegexes_doesNotMatch() {
            try {
                assertThrows<Panic>(".*") { throw Panic("1\n2") }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun moreRegexesThanLines_doesNotMatch() {
            try {
                assertThrows<Panic>(".*", ".*") { throw Panic("1") }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }
    }

    inner class AssertFailure {
        @Test
        fun exception_isSuccess() {
            assertFailure {
                throw Panic("Error")
            }
        }

        @Test
        fun assertionFailure_isSuccess() {
            assertFailure {
                fail("Error")
            }
        }

        @Test
        fun nothingThrown_isError() {
            try {
                assertFailure {}
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }

        @Test
        fun wrongMessage_isError() {
            try {
                assertFailure("expected message") { throw Panic("other message") }
            } catch (x: AssertionError) {
                return
            }

            fail("failure expected")
        }
    }
}