package straightway.testing.js

import kotlin.reflect.KFunction
import kotlin.test.fail

object CallSequence {
    interface MockVerification {
        operator fun String.invoke(vararg params: Any?)
        operator fun KFunction<*>.invoke(vararg params: Any?)
    }

    interface Verification {
        operator fun Mock<*>.invoke(block: MockVerification.() -> Unit)
        operator fun ExpectedCall.unaryPlus()
    }
}

fun verifySequence(block: CallSequence.Verification.() -> Unit): Collection<MockCall> {
    val expectedCalls = mutableListOf<ExpectedCall>()
    object : CallSequence.Verification {
        override fun Mock<*>.invoke(block: CallSequence.MockVerification.() -> Unit) {
            val mock = this
            object : CallSequence.MockVerification {
                override fun String.invoke(vararg params: Any?) { +ExpectedCall(mock, this, *params) }
                override fun KFunction<*>.invoke(vararg params: Any?) = name.invoke(*params)
            }.block()
        }
        override fun ExpectedCall.unaryPlus() {
            expectedCalls.add(this)
        }
    }.block()

    return verifyCallSequence(expectedCalls)
}

private fun verifyCallSequence(expectedCalls: Iterable<ExpectedCall>): Collection<MockCall> {
    val mocks = expectedCalls.map { it.mock }.distinct()
    val callSequence = mocks.flatMap { mock ->
        mock.calls.map { MockWithCall(mock, it) }
    }.sortedBy {
        it.call.sequenceNumber
    }

    try {
        return callSequence.assertSequence(expectedCalls.toList()).map { it.call }
    } catch (x: AssertionError) {
        fail("${x.message}${if (callSequence.any()) "\n  ${callSequence.joinToString("\n  ")}" else ""}")
    }
}

private class MockWithCall(val mock: Mock<*>, val call: MockCall) {
    override fun toString() = "$call ($mock)"
}

private fun Collection<MockWithCall>.assertSequence(expectedCalls: Collection<ExpectedCall>): Collection<MockWithCall> =
        when {
            expectedCalls.isEmpty() -> this
            isEmpty() -> fail("Could not find calls for" +
                    if (expectedCalls.any()) " ${expectedCalls.joinToString(", ")}" else "")
            else -> {
                val firstExpectedCall = expectedCalls.first()
                val firstActualCall = first()
                firstExpectedCall.predicate(firstActualCall.call).let {
                    if (firstActualCall.mock ===firstExpectedCall.mock && it == true)
                        this.drop(1).assertSequence(expectedCalls.drop(1))
                    else this.drop(1).assertSequence(expectedCalls)
                }
            }
        }
