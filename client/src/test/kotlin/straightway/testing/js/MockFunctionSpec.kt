package straightway.testing.js

import kotlin.test.assertFalse

data class MockFunctionSpec(private val mock: Mock<*>, private val times: Int?) {

    fun assertCalledNumberOfTimes(call: ExpectedCall) =
            call.apply {
                assertAndGetRemainingCalls().assertRemainingCalls(this)
            }

    private fun ExpectedCall.assertAndGetRemainingCalls() =
            if (times == 0)
                this@MockFunctionSpec.mock.calls.filter { this.predicate(it) == true }
            else
                verifySequence {
                    repeat(times ?: 1) { +this@assertAndGetRemainingCalls }
                }

    private fun Collection<MockCall>.assertRemainingCalls(expectedCall: ExpectedCall) =
            assertFalse(
                    times != null && any { expectedCall.predicate(it) == true },
                    "Expected $times calls, additionally found:\n  " +
                            joinToString("\n  "))
}