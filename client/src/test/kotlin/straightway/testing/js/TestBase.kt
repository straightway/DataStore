package straightway.testing.js

import kotlinx.coroutines.*
import kotlin.browser.window
import kotlin.coroutines.CoroutineContext
import kotlin.coroutines.EmptyCoroutineContext
import kotlin.js.Promise
import kotlin.test.AfterTest
import kotlin.test.BeforeTest
import kotlin.test.assertTrue
import kotlin.test.fail

open class TestBase {

    private var _tearDownAction: () -> Unit = {}
    private val _uncaughtExceptions = mutableListOf<Throwable>()

    val uncaughtExceptions: List<Throwable> get() = _uncaughtExceptions

    open fun setUp() {}
    open fun tearDown() {}

    fun asyncTest(block: suspend () -> Unit): dynamic {
        val tearDown = _tearDownAction
        _tearDownAction = {}

        return GlobalScope.promise {
            block()
            assertTrue(
                    uncaughtExceptions.isEmpty(),
                    "Uncaught exceptions:\n${uncaughtExceptions.joinToString("\n")}")
        }.then {
            tearDown()
            _uncaughtExceptions.clear()
        }
    }

    fun exception(x: Throwable) = _uncaughtExceptions.add(x)

    @BeforeTest
    private fun _setUp() {
        _tearDownAction = this::tearDown
        setUp()
    }

    @AfterTest
    private fun _tearDown() {
        _tearDownAction()
        _uncaughtExceptions.clear()
        _tearDownAction = {}
    }
}