package straightway.testing.js

import kotlin.reflect.KFunction

interface Mock<TMocked: Any> {
    val calls: List<MockCall>
    val name: String
    val obj: TMocked

    var liveLogging: Boolean

    fun reset()

    interface SetupEnvironment {
        operator fun <T> String.invoke(vararg params: Any?, callback: (Array<out Any?>) -> T)
        operator fun <T> KFunction<*>.invoke(vararg params: Any?, callback: (Array<out Any?>) -> T)
    }

    fun setup(block: SetupEnvironment.() -> Unit)

    interface VerifyEnvironment {
        operator fun String.invoke(vararg params: Any?): ExpectedCall
        operator fun KFunction<*>.invoke(vararg params: Any?): ExpectedCall
        infix fun ExpectedCall.wasCalled(times: MockCallTimes)
    }

    fun verify(block: VerifyEnvironment.() -> Unit)

    companion object {
        inline operator fun <reified TMocked: Any>invoke(
                noinline setup: MockSetup.() -> TMocked
        ): Mock<TMocked> = MockImpl(null, TMocked::class, setup)

        inline operator fun <reified TMocked: Any>invoke(
                name: String,
                noinline setup: MockSetup.() -> TMocked
        ): Mock<TMocked> = MockImpl(name, TMocked::class, setup)
    }
}
