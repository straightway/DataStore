package straightway.testing.js

interface MockConstraint {
    fun matches(other: dynamic): Boolean
}

