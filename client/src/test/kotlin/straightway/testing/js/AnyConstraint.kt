package straightway.testing.js

val any: MockConstraint = AnyConstraint

private object AnyConstraint : MockConstraint {
    override fun matches(other: dynamic) = true
    override fun toString() = "any"
}