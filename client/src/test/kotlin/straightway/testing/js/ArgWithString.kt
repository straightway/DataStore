package straightway.testing.js

data class ArgWithString(val arg: Any?, val string: String) {
    override fun toString() = string
}