package straightway.testing.js

import kotlin.reflect.KClass

fun itIs(explanation: String? = null, predicate: MockParameterExplanation.(dynamic) -> Boolean) =
        itIs(Any::class, explanation, predicate)

inline fun <reified T> itIs(
        explanation: String? = null,
        noinline predicate: MockParameterExplanation.(T) -> Boolean = { true }) =
        itIs(T::class, explanation) { predicate(it) }

fun itIs(
        type: KClass<*>,
        explanation: String? = null,
        predicate: MockParameterExplanation.(dynamic) -> Boolean = { true }
): MockConstraint =
    object : MockConstraint, MockParameterExplanation {
        override var explanation: String? = explanation

        override fun matches(other: dynamic): Boolean {
            if (!type.isInstance(other)) {
                this.explanation = "Invalid type. Expected: $type, got: ${other::class}"
                return false
            }

            return predicate(other)
        }

        override fun toString() =
                if (this.explanation === null) "${type.simpleName}"
                else "${this.explanation}: ${type.simpleName}"
    }