package straightway.testing.js

import kotlin.reflect.KFunction

interface MockSetup {
    val toStringResult: String
    fun <T> call(func: String, vararg params: Any?, action: () -> T): T
    fun call(func: String, vararg params: Any?)
}

fun <T> MockSetup.call(func: KFunction<*>, vararg params: Any?, action: () -> T): T =
        call(func.name, *params) { action() }

fun MockSetup.call(func: KFunction<*>, vararg params: Any?) =
        call(func.name, *params)
