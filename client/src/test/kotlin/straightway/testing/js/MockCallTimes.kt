package straightway.testing.js

data class MockCallTimes(val number: Int?)

val Int.times get() = MockCallTimes(this)
val atLeastOnce = MockCallTimes(null)
val once = 1.times
val never = MockCallTimes(0)