package straightway.testing.js

class ExpectedCall(val mock: Mock<*>, val stringRep: String, val predicate: (MockCall) -> Any) {

    constructor(mock: Mock<*>, expectedFunc: String, vararg expectedParams: Any?) :
            this(mock, stringRep(expectedFunc, expectedParams), { it.isCallTo(expectedFunc, *expectedParams) })

    override fun toString() = stringRep

    companion object {
        private fun stringRep(func: String, params: Array<out Any?>) =
                "$func(${params.map { JSON.stringify(it) }.joinToString(", ")})"
    }
}