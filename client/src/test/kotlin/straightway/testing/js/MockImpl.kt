package straightway.testing.js

import kotlin.reflect.KClass
import kotlin.reflect.KFunction

class MockImpl<TMocked: Any>(
        name: String?,
        private val mockedType: KClass<*>, setup: MockSetup.() -> TMocked
): Mock<TMocked> {

    private val setups = mutableListOf<Pair<ExpectedCall, (Array<out Any?>) -> Any?>>()

    private companion object {
        var sequenceNumber = 0
    }

    override val name: String

    override var calls: List<MockCall> = listOf()
        get
        private set

    override val obj = SetupImpl().setup()

    override var liveLogging: Boolean = false

    override fun reset() { calls = listOf() }

    override fun setup(block: Mock.SetupEnvironment.() -> Unit) {
        object : Mock.SetupEnvironment {
            override operator fun <T> String.invoke(vararg params: Any?, callback: (Array<out Any?>) -> T) {
                setups.add(ExpectedCall(this@MockImpl, this, *params) to callback)
            }

            override fun <T> KFunction<*>.invoke(vararg params: Any?, callback: (Array<out Any?>) -> T) =
                    name.invoke(*params, callback = callback)
        }.block()
    }

    override fun verify(block: Mock.VerifyEnvironment.() -> Unit) {
        object : Mock.VerifyEnvironment {
            override fun String.invoke(vararg params: Any?) =
                    ExpectedCall(this@MockImpl, this, *params)
            override fun KFunction<*>.invoke(vararg params: Any?) = name.invoke(*params)
            override fun ExpectedCall.wasCalled(times: MockCallTimes) {
                MockFunctionSpec(mock, times.number).assertCalledNumberOfTimes(this)
            }
        }.block()
    }

    override fun toString() = "Mock<${mockedType.simpleName}>($name)"

    init {
        this.name = name ?: "${hashCode()}"
    }

    private inner class SetupImpl : MockSetup {

        override val toStringResult: String get() = this@MockImpl.toString()

        override fun <T> call(func: String, vararg params: Any?, action: () -> T): T =
                action().let {
                    registerCall(func, params, it).unsafeCast<T>()
                }

        override fun call(func: String, vararg params: Any?) { registerCall(func, params) }
    }

    private fun registerCall(func: String, params: Array<out Any?>, returnValue: Any? = Unit): Any? {
        val call = MockCall(func, params.toList(), returnValue, sequenceNumber++)
        if (liveLogging) println(call)
        calls += call
        val registeredAction = setups.lastOrNull { it.first.predicate(call) == true }?.second
        return if (registeredAction == null) returnValue else registeredAction(params)
    }
}
