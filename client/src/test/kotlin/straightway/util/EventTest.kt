/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.js.*
import straightway.testing.js.assertDoesNotThrow
import straightway.testing.js.assertThrows
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class EventTest {

    private interface Listener {
        fun onEvent(s: String)
    }

    private val test get() = Given {
        object {
            val sut = Event<String>()
            val listener = Mock<Listener> {
                object : Listener {
                    override fun onEvent(s: String) = call(Listener::onEvent, s)
                }
            }
        }
    }

    @Test
    fun firingAnEvent_withoutListeners_hasNoEffect() =
            test when_ { sut("Hello") } then {
                listener.verify {
                    (Listener::onEvent)(any) wasCalled never
                }
            }

    @Test
    fun firingAnEvent_withOneListener_notifiesItOnFiring() =
            test while_ {
                sut.attach(listener.obj::onEvent)
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)("Hello") wasCalled atLeastOnce
                }
            }

    @Test
    fun firingNnEvent_withTheSameListener_twice_notifiesItTwiceOnFiring() =
            test while_ {
                sut.attach(listener.obj::onEvent)
                sut.attach(listener.obj::onEvent)
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)("Hello") wasCalled 2.times
                }
            }

    @Test
    fun firingEnEvent_withDetachedListener_hasNoEffect() =
            test while_ {
                sut.detach(sut.attach(listener.obj::onEvent))
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)(any) wasCalled never
                }
            }

    @Test
    fun detachingUnconnectedHandler_hasNoEffect() =
            test when_ {
                sut.attach(listener.obj::onEvent)
            } then {
                assertDoesNotThrow { it.result }
            }

    @Test
    fun detachingLambdaAsHandlerViaToken() =
            test while_ {
                val token = sut.attach { listener.obj.onEvent(it) }
                sut.detach(token)
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)(any) wasCalled never
                }
            }

    @Test
    fun detachOnNotConnectedHandler_returnsFalse() =
            test when_ { sut.detach(EventHandlerToken()) } then { assertFalse(it.result) }

    @Test
    fun detachOnConnectedEvent_returnsTrue() =
            test andGiven {
                object {
                    val sut = it.sut
                    val token = sut.attach(it.listener.obj::onEvent)
                }
            } when_ {
                sut.detach(token)
            } then {
                assertTrue(it.result)
            }

    @Test
    fun attachingWhileEventIsExecuted_isNotConsideredInThisExecution() =
            test while_ {
                sut.attach { sut.attach(listener.obj::onEvent) }
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)(any) wasCalled never
                }
            }

    @Test
    fun attachingWhileEventEsExecuted_isConsideredInTheNextExecution() =
            test while_ {
                sut.attach { sut.attach(listener.obj::onEvent) }
            } when_ {
                sut("Hello")
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)("Hello") wasCalled once
                }
            }

    @Test
    fun recursiveInvocationThrows() =
            test while_ {
                var token = EventHandlerToken()
                token = sut.attach { sut.detach(token); sut("Hello") }
            } when_ {
                sut("Hello")
            } then {
                assertThrows<Panic> { it.result }
            }

    @Test
    @Suppress("SwallowedException")
    fun anotherInvocation_afterFirstInvocationThre_works() =
            test while_ {
                val token = sut.attach { throw Panic("Aaaah!") }
                try { sut("Hello") } catch (e: Panic) {}
                sut.detach(token)
            } when_ {
                sut("Hello")
            } then {
                assertDoesNotThrow { it.result }
            }

    @Test
    fun handleOnce_handlesTheEvent() =
            test while_ {
                sut.handleOnce { listener.obj.onEvent(it) }
            } when_ {
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)("Hello") wasCalled once
                }
            }

    @Test
    fun handleOnce_detachesItself_afterHandlingTheEvent() =
            test while_ {
                sut.handleOnce { listener.obj.onEvent(it) }
            } when_ {
                sut("Hello")
                sut("Hello")
            } then {
                listener.verify {
                    (Listener::onEvent)("Hello") wasCalled once
                }
            }

    @Test
    fun handleOnce_returnsTokenAllowingToFetachManually() {
        lateinit var token: EventHandlerToken
        test while_ {
            token = sut.handleOnce { listener.obj.onEvent(it) }
        } when_ {
            sut.detach(token)
            sut("Hello")
        } then {
            listener.verify {
                (Listener::onEvent)(any) wasCalled never
            }
        }
    }
}