/*
 * Copyright 2016 straightway
 *
 *  Licensed under the Apache License, Version 2.0 (the &quot;License&quot;);
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an &quot;AS IS&quot; BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package straightway.utils

import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.js.*
import straightway.testing.js.assertDoesNotThrow
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class AsyncEventTest : TestBase() {

    private interface Listener {
        suspend fun onEvent(s: String)
    }

    private val test get() = Given {
        object {
            val sut = AsyncEvent<String>()
            val listener = Mock<Listener> {
                object : Listener {
                    override suspend fun onEvent(s: String) = call(Listener::onEvent, s)
                }
            }
        }
    }

    @Test
    fun firingAnEvent_withoutListeners_hasNoEffect() = asyncTest {
        test whenAsync { sut("Hello") } then {
            listener.verify {
                "onEvent"(any) wasCalled never
            }
        }
    }

    @Test
    fun firingAnEvent_withOneListener_notifiesItOnFiring() = asyncTest {
        test while_ {
            sut.attach { listener.obj.onEvent(it) }
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"("Hello") wasCalled once
            }
        }
    }

    @Test
    fun firingEvent_withTheSameListener_twice_notifiesItTwiceOnFiring() = asyncTest {
        test while_ {
            sut.attach { listener.obj.onEvent(it) }
            sut.attach { listener.obj.onEvent(it) }
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"("Hello") wasCalled 2.times
            }
        }
    }

    @Test
    fun firingEnEvent_withDetachedListener_hasNoEffect() = asyncTest {
        test while_ {
            sut.detach (sut.attach { listener.obj.onEvent(it) })
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"(any) wasCalled never
            }
        }
    }

    @Test
    fun detachingUnconnectedHandler_hasNoEffect() = asyncTest {
        test whenAsync {
            sut.attach(listener.obj::onEvent)
        } then {
            assertDoesNotThrow { it.result }
        }
    }

    @Test
   fun detachingLambdaAsHandlerViaToken() = asyncTest {
        test while_ {
            val token = sut.attach { listener.obj.onEvent(it) }
            sut.detach(token)
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"(any) wasCalled never
            }
        }
    }

    @Test
   fun detachOnNotConnectedHandler_returnsFalse() = asyncTest {
        test whenAsync { sut.detach(EventHandlerToken()) } then { assertFalse(it.result) }
    }

    @Test
   fun detachOnConnectedEvent_returnsTrue() = asyncTest {
        test andGiven {
            object {
                val sut = it.sut
                val token = sut.attach { p -> it.listener.obj.onEvent(p) }
            }
        } whenAsync {
            sut.detach(token)
        } then {
            assertTrue(it.result)
        }
    }

    @Test
   fun attachingWhileEventIsExecuted_isNotConsideredInThisExecution() = asyncTest {
        test while_ {
            sut.attach { sut.attach(listener.obj::onEvent) }
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"(any) wasCalled never
            }
        }
    }

    @Test
   fun attachingWhileEventEsExecuted_isConsideredInTheNextExecution() = asyncTest {
        test while_ {
            sut.attach { sut.attach(listener.obj::onEvent) }
        } whenAsync {
            sut("Hello")
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"("Hello") wasCalled once
            }
        }
    }

    @Test
    @Suppress("SwallowedException")
    fun anotherInvocation_afterFirstInvocationThre_works() = asyncTest {
        test whileAsync {
            val token = sut.attach { throw Panic("Aaaah!") }
            try {
                sut("Hello")
            } catch (e: Panic) {
            }
            sut.detach(token)
        } whenAsync {
            sut("Hello")
        } then {
            assertDoesNotThrow { it.nullableResult }
        }
    }

    @Test
    fun handleOnce_handlesTheEvent() = asyncTest {
        test while_ {
            sut.handleOnce { listener.obj.onEvent(it) }
        } whenAsync {
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"("Hello") wasCalled once
            }
        }
    }

    @Test
    fun handleOnce_detachesItself_afterHandlingTheEvent() = asyncTest {
        test while_ {
            sut.handleOnce { listener.obj.onEvent(it) }
        } whenAsync {
            sut("Hello")
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"("Hello") wasCalled once
            }
        }
    }

    @Test
    fun handleOnce_returnsTokenAllowingToFetachManually() = asyncTest {
        lateinit var token: EventHandlerToken
        test while_ {
            token = sut.handleOnce { listener.obj.onEvent(it) }
        } whenAsync {
            sut.detach(token)
            sut("Hello")
        } then {
            listener.verify {
                "onEvent"(any) wasCalled never
            }
        }
    }
}