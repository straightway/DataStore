package straightway.datastore

import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals

class MessageCollectionTest {

    @Test
    fun serialization() {
        val newMessage = ChatMessage("dir", "Text")
        val data = MessageCollection()
        data.messages.add(newMessage)
        val expected = json(
                "message" to arrayOf(
                        json("body" to "Text", "date_added" to newMessage.dateAdded, "directory" to "dir")
                )
        )

        assertEquals(JSON.stringify(expected), JSON.stringify(data))
    }
}