package straightway.datastore

import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.js.*
import straightway.utils.AsyncEvent
import zeronet.*
import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertNull

class DataStoreTest : TestBase() {

    private companion object {
        private val DATA_PATH = "data/users"
        private val DATA_FILE_NAME = "data.bin"
    }

    private suspend fun test() = Given {
        val issueCommandEventObj = AsyncEvent<String>()
        val deleteMessageEventObj = AsyncEvent<String>()
        val rePublishEventObj = AsyncEvent<String>()
        object {
            suspend fun issueCommand(cmd: String) { issueCommandEventObj(cmd) }
            suspend fun deleteMessage(msg: String) { deleteMessageEventObj(msg) }
            suspend fun rePublish(id: String) { rePublishEventObj(id) }
            val zeroFrameMock = ZeroFrameMock().apply {
                obj.setCommandResult("ok")
                obj.setCommandResult(
                        "createSelfSignedIdentity",
                        btoa(JSON.stringify(Cert(authAddress = "anonymousAddress"))))
                obj.setCommandResult("serverInfo", ServerInfo())
                obj.setCommandResult("siteInfo", SiteInfo(address = "siteAddress"))
                obj.setCommandResult("fileList", arrayOf("anonymousAddress/data.bin"))
                obj.setCommandResult("fileGet", "Hello")
            }
            val uiMock = Mock<DataStoreUI> {
                object : DataStoreUI {
                    override val issueCommandEvent = issueCommandEventObj
                    override val deleteMessageEvent = deleteMessageEventObj
                    override val rePublishEvent = rePublishEventObj
                    override fun clearMessageInput() =
                            call(DataStoreUI::clearMessageInput)
                    override fun addMessage(id: String, messageText: String) =
                            call(DataStoreUI::addMessage, id, messageText)
                    override fun clearMessages() =
                            call(DataStoreUI::clearMessages)
                }
            }
            val sut = DataStore(zeroFrameMock.obj, uiMock.obj)
        }
    }

    inner class OnOpenWebsocket {

        @Test
        fun requestsPluginInstallation_whenSpecialPlugin_isNotInstalled() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setCommandResult("serverInfo", ServerInfo(pluginsRev = json()))
                zeroFrameMock.obj.openWebSocketEvent(Unit)
            } then {
                zeroFrameMock.verifyCommandCall(
                        "pluginAddRequest",
                        "plugins/Straightway")
            }
        }

        @Test
        fun requestsPluginInstallation_whenSpecialPlugin_hasWrongVersion() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setCommandResult(
                        "serverInfo",
                        ServerInfo(pluginsRev = json("Straightway" to Int.MAX_VALUE)))
                zeroFrameMock.obj.openWebSocketEvent(Unit)
            } then {
                zeroFrameMock.verifyCommandCall(
                        "pluginAddRequest",
                        "plugins/Straightway")
            }
        }

        @Test
        fun doesNotRequestsPluginInstallation_whenSpecialPlugin_hasCorrectVersion() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setCommandResult(
                        "serverInfo",
                        ServerInfo(pluginsRev = json("Straightway" to 1)))
                zeroFrameMock.obj.openWebSocketEvent(Unit)
            } then {
                zeroFrameMock.verify {
                    "cmd"("pluginAddRequest", any) wasCalled never
                }
            }
        }

        @Test
        fun getsAllDataFilesFromTheServer() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.openWebSocketEvent(Unit)
            } then {
                zeroFrameMock.run {
                    verifyCommandCall("fileList", "$DATA_PATH")
                }
            }
        }

        @Test
        fun displaysAllMessages() = asyncTest {
            test() whenAsync {
                with(zeroFrameMock.obj)
                {
                    setCommandResult(
                            "fileList",
                            arrayOf("directory/$DATA_FILE_NAME", "content.json"))
                    setCommandResult("fileGet", "Hello")
                    openWebSocketEvent(Unit)
                }
            } then {
                verifySequence {
                    uiMock {
                        "clearMessages"()
                        "addMessage"("directory", "Hello")
                    }
                }
            }
        }

        @Test
        fun retrievesSiteInfo() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.openWebSocketEvent(Unit)
            } then {
                zeroFrameMock.assertZeroFrameCommandWithReturn("siteInfo")
            }
        }
    }

    inner class OnIssueCommand {
        private suspend fun test() =
                this@DataStoreTest.test() whileAsync {
                    zeroFrameMock.obj.openWebSocketEvent(Unit)
                }

        @Test
        fun storeCommand_isProperlyExecuted() = asyncTest {
            test() whenAsync {
                issueCommand("store Hello")
            } then {
                zeroFrameMock.verifyCommandCall(
                        "fileWrite",
                        "$DATA_PATH/anonymousAddress/$DATA_FILE_NAME",
                        btoa("Hello"))
            }
        }

        @Test
        fun invalidCommand_issuesError() = asyncTest {
            test() whenAsync {
                issueCommand("Invalid Command")
            } then {
                zeroFrameMock.verifyCommandFireAndForget(
                        "wrapperNotification",
                        "error",
                        "Invalid command \"Invalid\" in: Invalid Command")
            }
        }
    }

    inner class OnSiteInfoSet {
        @Test
        fun onSiteInfoSet_setsSiteInfo() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setSiteInfoEvent(createSiteInfoEvent(address = "address"))
                issueCommand("store Hello")
            } then {
                zeroFrameMock.verify {
                    (ZeroFrame::fireAndForget)("wrapperNotification", any) wasCalled never
                }
            }
        }

        @Test
        fun fileDoneEvent_displaysAllMessages() = asyncTest {
            test() whileAsync {
                zeroFrameMock.obj.setCommandResult("fileList", arrayOf("msgDir/data.bin"))
                zeroFrameMock.obj.setCommandResult("fileGet", "msgText")
            } whenAsync {
                zeroFrameMock.obj.setSiteInfoEvent(createSiteInfoEvent(event = arrayOf("file_done")))
            } then {
                uiMock.verify {
                    (DataStoreUI::addMessage)("msgDir", "msgText") wasCalled once
                }
            }
        }

        @Test
        fun nullEvent_doNotDisplaysAllMessages() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setSiteInfoEvent(createSiteInfoEvent(event = null))
            } then {
                zeroFrameMock.verify {
                    "cmd"("fileQuery", any) wasCalled never
                }
            }
        }

        @Test
        fun emptyEvent_doNotDisplaysAllMessages() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setSiteInfoEvent(createSiteInfoEvent(event = arrayOf()))
            } then {
                zeroFrameMock.verify {
                    "cmd"("fileQuery", any) wasCalled never
                }
            }
        }

        @Test
        fun noFileDoneEvent_doNotDisplaysAllMessages() = asyncTest {
            test() whenAsync {
                zeroFrameMock.obj.setSiteInfoEvent(createSiteInfoEvent(event = arrayOf("other event")))
            } then {
                zeroFrameMock.verify {
                    "cmd"("fileQuery", any) wasCalled never
                }
            }
        }

        private fun createSiteInfoEvent(address: String = "", event: Array<String>? = null): SiteInfoEvent {
            val result = SiteInfo(address = address).asDynamic()
            result.event = event
            return result.unsafeCast<SiteInfoEvent>()
        }
    }

    inner class Store {

        private val certBase64 = btoa(JSON.stringify(Cert(
                authAddress = "certAuthAddress",
                authPrivateKey = "certPrivateKey")))

        private suspend fun test() = this@DataStoreTest.test() whileAsync {
            zeroFrameMock.obj.setCommandResult("siteInfo", SiteInfo(address = "siteAddress"))
            zeroFrameMock.obj.setCommandResult("createSelfSignedIdentity", certBase64)
            zeroFrameMock.obj.setCommandResult("fileList", arrayOf("msgDir/data.bin"))
            zeroFrameMock.obj.setCommandResult("fileGet", "msgText")
            zeroFrameMock.obj.openWebSocketEvent(Unit)
        }

        @Test
        fun isDefaultAction() = asyncTest {
            test() whenAsync {
                issueCommand("Hello")
            } then {
                verifySequence {
                    +zeroFrameMock.wasCommandCalledWith(
                            "sitePublish",
                            "sign" to false,
                            "privatekey" to "certPrivateKey",
                            "inner_path" to "$DATA_PATH/certAuthAddress/content.json")
                }
            }
        }

        @Test
        fun usesSelfSignedCertificate() = asyncTest {
            test() whenAsync {
                issueCommand("store Hello")
            } then {
                assertNull(it.exception)
                verifySequence {
                    +zeroFrameMock.wasCommandCalledWith("createSelfSignedIdentity")
                    +zeroFrameMock.wasCommandCalledWith("setTempCert", "siteAddress", certBase64)
                    +zeroFrameMock.wasCommandCalledWith(
                            "fileWrite",
                            "$DATA_PATH/certAuthAddress/$DATA_FILE_NAME",
                            btoa("Hello"))
                    +zeroFrameMock.wasCommandCalledWith(
                            "siteSign",
                            "remove_missing_optional" to false,
                            "privatekey" to "certPrivateKey",
                            "inner_path" to "$DATA_PATH/certAuthAddress/content.json")
                    +zeroFrameMock.wasCommandCalledWith(
                            "sitePublish",
                            "sign" to false,
                            "privatekey" to "certPrivateKey",
                            "inner_path" to "$DATA_PATH/certAuthAddress/content.json")
                    +zeroFrameMock.wasCommandCalledWith("setTempCert", "siteAddress", null)
                }
            }
        }

        @Test
        fun updatesMessagesInUI() = asyncTest {
            test() whenAsync {
                issueCommand("store Hello")
            } then {
                verifySequence {
                        +zeroFrameMock.wasCommandCalledWith("setTempCert", any, itIs { it != null })
                        uiMock {
                            (DataStoreUI::clearMessages)()
                            (DataStoreUI::addMessage)("msgDir", "msgText")
                        }
                        +zeroFrameMock.wasCommandCalledWith("setTempCert", any, null)
                }
            }
        }

        @Test
        fun clearsInputField() = asyncTest {
            test() whenAsync {
                issueCommand("store Hello")
            } then {
                uiMock.verify { (DataStoreUI::clearMessageInput)() wasCalled once }
            }
        }

        @Test
        fun exceptionAfterSettingTempCert_resetsTempCert() = asyncTest {
            test() whileAsync {
                uiMock.setup {
                    (DataStoreUI::clearMessages) { throw Panic("Exception") }
                }
            } whenAsync {
                issueCommand("store Hello")
            } then {
                verifySequence {
                    with(zeroFrameMock) {
                        +wasCommandCalledWith("setTempCert", any, itIs { it != null })
                        +wasCommandCalledWith("setTempCert", any, null)
                    }
                }
            }
        }

        @Test
        fun publishFailed_showsMessage() = asyncTest {
            test() whileAsync {
                zeroFrameMock.setup {
                    "cmd"("sitePublish", any) { throw Panic("Publish failed") }
                }
            } whenAsync {
                issueCommand("store Hello")
            } then {
                verifySequence {
                    with(zeroFrameMock) {
                        +wasCommandCalledWith("setTempCert", any, itIs { it != null })
                        +wasCommandFiredWith(
                                "wrapperNotification",
                                "error",
                                "Publication error: \"Publish failed\"")
                        +wasCommandCalledWith("setTempCert", any, null)
                    }
                }
            }
        }
    }

    inner class Delete {

        @Test
        fun deletesMessageLocallyAndUpdatesMessages() = asyncTest {
            test() whenAsync {
                deleteMessage("msgDir")
            } then {
                verifySequence {
                    +zeroFrameMock.wasCommandCalledWith("deleteLocally", "$DATA_PATH/msgDir")
                    uiMock { (DataStoreUI::clearMessages)() } // part of display
                }
            }
        }
    }

    inner class Load {
        @Test
        fun downloadsFile() = asyncTest {
            test() whenAsync {
                issueCommand("load dataID")
            } then {
                zeroFrameMock.verifyCommandCall("fileNeed", "$DATA_PATH/dataID/$DATA_FILE_NAME", any)
            }
        }

        @Test
        fun clearsInput() = asyncTest {
            test() whenAsync {
                issueCommand("load dataID")
            } then {
                uiMock.verify {
                    (DataStoreUI::clearMessageInput)() wasCalled once
                }
            }
        }

        @Test
        fun notifiesErrors() = asyncTest {
            test() whileAsync {
                zeroFrameMock.setup {
                    "cmd"("fileNeed", any) { throw Panic("Could not get file") }
                }
            } whenAsync {
                issueCommand("load dataID")
            } then {
                zeroFrameMock.verifyCommandFireAndForget("wrapperNotification", "error", any)
            }
        }
    }

    inner class Get {
        @Test
        fun withUnknownItem_showsErrorMessage() = asyncTest {
            test() whenAsync {
                issueCommand("get unknown item")
            } then {
                zeroFrameMock.verifyCommandFireAndForget(
                        "wrapperNotification",
                        "info",
                        "unknown item=<unknown>",
                        5000)
            }
        }

        @Test
        fun clearsInput() = asyncTest {
            test() whenAsync {
                issueCommand("get loadTimeout")
            } then {
                uiMock.verify {
                    (DataStoreUI::clearMessageInput)() wasCalled once
                }
            }
        }
    }

    inner class Set {
        @Test
        fun withUnknownItem_showsErrorMessage() = asyncTest {
            test() whenAsync {
                issueCommand("set unknownItem=3")
            } then {
                zeroFrameMock.verifyCommandFireAndForget(
                        "wrapperNotification",
                        "error",
                        "cannot set unknownItem")
            }
        }

        @Test
        fun clearsInput() = asyncTest {
            test() whenAsync {
                issueCommand("set loadTimeout=100")
            } then {
                uiMock.verify {
                    (DataStoreUI::clearMessageInput)() wasCalled once
                }
            }
        }
    }

    inner class Config_loadTimeout {
        @Test
        fun defaultValue() = asyncTest {
            test() whenAsync {
                issueCommand("load dataID")
            } then {
                zeroFrameMock.verifyCommandCall("fileNeed", any, 300)
            }
        }

        @Test
        fun set_viaCommand() = asyncTest {
            test() whileAsync {
                issueCommand("set loadTimeout=100")
            } whenAsync {
                issueCommand("load dataID")
            } then {
                zeroFrameMock.verifyCommandCall("fileNeed", any, 100)
                zeroFrameMock.verifyShortInfo("loadTimeout=100")
            }
        }

        @Test
        fun set_invalidConfigSpec_issuesError() = asyncTest {
            test() whileAsync {
                issueCommand("set invalid")
            } whenAsync {
                issueCommand("load dataID")
            } then {
                zeroFrameMock.verifyCommandFireAndForget("wrapperNotification", "error", any)
            }
        }

        @Test
        fun get_viaCommand() = asyncTest {
            test() whenAsync {
                issueCommand("get loadTimeout")
            } then {
                zeroFrameMock.verifyShortInfo("loadTimeout=300")
            }
        }
    }

    inner class Cmd {
        @Test
        fun issesZeroNetCommand() = asyncTest {
            test() whenAsync {
                issueCommand("cmd cmdID [\"param1\", 83]")
            } then {
                zeroFrameMock.verify {
                    "cmd"("cmdID", arrayOf("param1", 83)) wasCalled once
                }
            }
        }

        @Test
        fun logsResult() = asyncTest {
            test() while_ {
                zeroFrameMock.obj.setCommandResult("cmdId", arrayOf(1, 2, 3))
            } whenAsync {
                issueCommand("cmd cmdId [\"param1\", 83]")
            } then {
                zeroFrameMock.obj.loggerMock.verify {
                    (Logger::log)(Logger.Level.Info, "DataStore", "cmdId -> [1,2,3]") wasCalled once
                }
            }
        }

        @Test
        fun displaysResult() = asyncTest {
            test() while_ {
                zeroFrameMock.obj.setCommandResult("cmdId", arrayOf(1, 2, 3))
            } whenAsync {
                issueCommand("cmd cmdId [\"param1\", 83]")
            } then {
                zeroFrameMock.verifyShortInfo("cmdId -> [1,2,3]")
            }
        }
    }

    inner class RePublish {
        @Test
        fun publishesItemWithoutSigning() = asyncTest {
            test() whenAsync {
                rePublish("id")
            } then {
                zeroFrameMock.verifyCommandCall(
                        "sitePublish",
                        "sign" to false,
                        "inner_path" to "$DATA_PATH/id/content.json")
            }
        }
    }

    private fun String.isChatMessage(isOk: ChatMessage.() -> Boolean = { true }) =
            decodeChatMessage().isOk()

    private fun String.decodeChatMessage(): ChatMessage =
            JSON.parse<dynamic>(atob(this)).message[0].unsafeCast<ChatMessage>()

    private fun Mock<ZeroFrameMock>.verifyShortInfo(msg: String) =
            verifyCommandFireAndForget("wrapperNotification", "info", msg, 5000)
}
