package zeronet

import straightway.testing.js.TestBase
import kotlin.test.Test
import kotlin.js.json

class DbTest : TestBase() {

    @Test
    fun query_passesProperParameters_toZeroFrame() = asyncTest {
        testZeroFrameCommandWithReturnValue(arrayOf(1, 2, 3)) whenAsync {
            obj.db.query<Int>("Query", "Arg0" to 1, "Arg1" to "Two")
        } then {
            assertZeroFrameCommandWithReturn(
                    "dbQuery",
                    args = arrayOf("Query", json("Arg0" to 1, "Arg1" to "Two")),
                    result = it.result)
        }
    }
}