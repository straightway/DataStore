package zeronet

import kotlin.test.Test
import kotlin.test.assertEquals

class UtilitiesTest {

    @Test
    fun notNullNamedParameters_noParameters_returnsEmptyResult() {
        assertEquals(0, notNullNamedParameters().size)
    }

    @Test
    fun notNullNamedParameters_nullValue_isOmitted() {
        assertEquals(0, notNullNamedParameters("Key" to null).size)
    }

    @Test
    fun notNullNamedParameters_nnotNullValue_isReturned() {
        assertEquals(listOf("Key" to "Value"), notNullNamedParameters("Null" to null, "Key" to "Value").toList())
    }
}