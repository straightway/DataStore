package zeronet

import straightway.error.Panic
import straightway.testing.bdd.Given
import straightway.testing.js.*
import kotlin.js.json
import kotlin.test.*

class ZeroFrameTest : TestBase() {

    private val test get() = Given {
        ZeroFrameMock()
    }

    @Test
    fun cmd_withPositionalParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        test whenAsync {
            obj.setCommandResult(83)
            obj.cmd<Int>("CMD", 3.14, "Hello")
        } then {
            assertEquals(83, it.result)
            verify {
                "cmd"("CMD", itIs { JSON.stringify(it) == "[3.14,\"Hello\"]" }) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmd_withNamedParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        test whenAsync {
            obj.setCommandResult(83)
            obj.cmd<Int>("CMD", "pi" to 3.14, "string" to "Hello")
        } then {
            assertEquals(83, it.result)
            verify {
                "cmd"("CMD", json("pi" to 3.14, "string" to "Hello")) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmd_withoutResult_withPositionalParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        test whenAsync {
            obj.setCommandResult(Unit)
            obj.cmd<Unit>("CMD", 3.14, "Hello")
        } then {
            verify {
                "cmd"("CMD", itIs { JSON.stringify(it) == "[3.14,\"Hello\"]" }) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmd_withoutResult_withNamedParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        test whenAsync {
            obj.setCommandResult(Unit)
            obj.cmd<Unit>("CMD", "pi" to 3.14, "string" to "Hello")
        } then {
            verify {
                "cmd"("CMD", json("pi" to 3.14, "string" to "Hello")) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withPositionalParameters_returningOK_doesNotThrow() = asyncTest {
        test whenAsync {
            obj.setCommandResult("ok")
            obj.cmdThrow("CMD", 3.14, "Hello")
        } then {
            verify {
                "cmd"("CMD", itIs { JSON.stringify(it) == "[3.14,\"Hello\"]" }) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withPositionalParameters_notReturningOK_throws() = asyncTest {
        test whenAsync {
            obj.setCommandResult("Failure")
            obj.cmdThrow("CMD", 3.14, "Hello")
        } then {
            assertFails { it.nullableResult }
            verify {
                "cmd"("CMD", itIs { JSON.stringify(it) == "[3.14,\"Hello\"]" }) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withSingleAnyParameter_returningOK_doesNotThrow() = asyncTest {
        test whenAsync {
            obj.setCommandResult("ok")
            obj.cmdThrow("CMD", "Lalala")
        } then {
            verify {
                "cmd"("CMD", "Lalala") wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withSingleAnyParameter_notReturningOK_throws() = asyncTest {
        test whenAsync {
            obj.setCommandResult("Failure")
            obj.cmdThrow("CMD", "Lalala")
        } then {
            assertFails { it.nullableResult }
            verify {
                "cmd"("CMD", "Lalala") wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withNamedParameters_returningOK_doesNotThrow() = asyncTest {
        test whenAsync {
            obj.setCommandResult("ok")
            obj.cmdThrow("CMD", json("pi" to 3.14, "string" to "Hello"))
        } then {
            verify {
                "cmd"("CMD", json("pi" to 3.14, "string" to "Hello")) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun cmdThrow_withNamedParameters_notReturningOK_throws() = asyncTest {
        test whenAsync {
            obj.setCommandResult("Failure")
            obj.cmdThrow("CMD", json("pi" to 3.14, "string" to "Hello"))
        } then {
            assertFails { it.nullableResult }
            verify {
                "cmd"("CMD", json("pi" to 3.14, "string" to "Hello")) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun fireAndForget_withPositionalParameters_issuesProperCommand() {
        test when_ {
            obj.fireAndForget("CMD", 3.14, "Hello")
        } then {
            verify {
                (ZeroFrame::fireAndForget)("CMD", itIs { JSON.stringify(it) == "[3.14,\"Hello\"]" }) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun fireAndForget_withNamedParameters_issuesProperCommand() {
        test when_ {
            obj.fireAndForget("CMD", json("pi" to 3.14, "string" to "Hello"))
        } then {
            verify {
                (ZeroFrame::fireAndForget)("CMD", json("pi" to 3.14, "string" to "Hello")) wasCalled atLeastOnce
            }
        }
    }

    @Test
    fun doNotifyingExceptionsAsync_executesBlock_withoutNotification() = asyncTest {
        var calls = 0
        test whenAsync {
            obj.doNotifyingExceptionsAsync { ++calls }
        } then {
            assertEquals(1, calls)
        }
    }

    @Test
    fun doNotifyingExceptionsAsync_notifiesUser_ofException() = asyncTest {
        test whenAsync {
            obj.doNotifyingExceptionsAsync { throw Panic("TestError") }
        } then {
            verify {
                (ZeroFrame::fireAndForget)(
                "wrapperNotification",
                itIs {
                    val error = JSON.stringify(it)
                    error.contains("Unexpected exception:") && error.contains("TestError")
                }) wasCalled atLeastOnce
            }
        }
    }
}