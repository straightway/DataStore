package zeronet

import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals

class SiteCacheTest {
    private val sut = json(
            "time_modified_files_check" to 3.14,
            "modified_files" to arrayOf("a", "b", "c")
    ).unsafeCast<SiteCache>()

    @Test
    fun fieldsAreFilledAsExpected() {
        assertEquals(3.14, sut.timeModifiedFilesCheck)
        assertEquals(listOf("a", "b", "c"), sut.modifiedFiles.toList())
    }
}