package zeronet

import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals

class SiteInfoTest {
    private val sut = json(
            "auth_key" to "ValueOfauth_key",
            "auth_address" to "ValueOfauth_address",
            "cert_user_id" to "ValueOfcert_user_id",
            "address" to "ValueOfaddress",
            "address_short" to "ValueOfaddress_short",
            "settings" to json("ajax_key" to "ValueOfajax_key"),
            "content_updated" to true,
            "bad_files" to 2,
            "size_limit" to 3,
            "next_size_limit" to 5,
            "peers" to 7,
            "started_task_num" to 11,
            "tasks" to 13,
            "workers" to 17,
            "content" to json("address" to "ValueOfaddress"),
            "privatekey" to false,
            "feed_follow_num" to 19
    ).unsafeCast<SiteInfo>()

    @Test
    fun fieldsAreFilledAsExpected() {
        assertEquals("ValueOfauth_key", sut.authKey)
        assertEquals("ValueOfauth_address", sut.authAddress)
        assertEquals("ValueOfcert_user_id", sut.certUserId)
        assertEquals("ValueOfaddress", sut.address)
        assertEquals("ValueOfaddress_short", sut.addressShort)
        assertEquals("ValueOfajax_key", sut.settings.ajaxKey)
        assertEquals(true, sut.contentUpdated)
        assertEquals(2, sut.badFiles)
        assertEquals(3, sut.sizeLimit)
        assertEquals(5, sut.nextSizeLimit)
        assertEquals(7, sut.peers)
        assertEquals(11, sut.startedTaskNum)
        assertEquals(13, sut.tasks)
        assertEquals(17, sut.workers)
        assertEquals("ValueOfaddress", sut.content.address)
        assertEquals(false, sut.privateKey)
        assertEquals(19, sut.feedFollowNum)
    }
}