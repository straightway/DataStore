package zeronet

import straightway.testing.js.*
import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertFails
import kotlin.test.assertNotNull
import kotlin.test.assertTrue

class FileTest : TestBase() {

    inner class Read {
        @Test
        fun passesProperParameters_toZeroFrame() = asyncTest {
            testZeroFrameCommandWithReturnValue("Hello") whenAsync {
                obj.file.read("file", isRequired = false, format = File.Format.Base64, timeoutMS = 2000)
            } then {
                assertZeroFrameCommandWithReturn(
                        "fileGet",
                        args = json(
                                "inner_path" to "file",
                                "required" to false,
                                "format" to "base64",
                                "timeout" to 2000),
                        result = it.result)
            }
        }

        @Test
        fun defaultValue_isRequired_true() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok").whenAsync {
                obj.file.read("file")
            }.thenHasParameters("fileGet") {
                it["required"] == true
            }
        }

        @Test
        fun read_defaultValue_format_Text() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok").whenAsync {
                obj.file.read("file")
            }.thenHasParameters("fileGet") {
                it["format"] == "text"
            }
        }

        @Test
        fun defaultValue_timeout_300000ms() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok").whenAsync {
                obj.file.read("file")
            }.thenHasParameters("fileGet") {
                it["timeout"] == 300
            }
        }
    }

    inner class Write {
        @Test
        fun passesProperParameters_toZeroFrame() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok") whenAsync {
                obj.file.write("file", "Content")
            } then {
                assertZeroFrameCommandWithReturn("fileWrite", args = arrayOf("file", "Content"))
            }
        }

        @Test
        fun throwsOnFailure() = asyncTest {
            testZeroFrameCommandWithReturnValue("Failure") whenAsync {
                obj.file.write("file", "Content")
            } then {
                assertNotNull(it.exception)
            }
        }
    }

    inner class Delete {
        @Test
        fun passesProperParameters_toZeroFrame() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok") whenAsync {
                obj.file.delete("file")
            } then {
                assertZeroFrameCommandWithReturn("fileDelete", args = arrayOf("file"))
            }
        }

        @Test
        fun throwsOnFailure() = asyncTest {
            testZeroFrameCommandWithReturnValue("Failure") whenAsync {
                obj.file.delete("file")
            } then {
                assertNotNull(it.exception)
            }
        }
    }

    inner class Need {
        @Test
        fun passesProperParameters_toZeroFrame() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok") whenAsync {
                obj.file.need("file", 100)
            } then {
                assertZeroFrameCommandWithReturn("fileNeed", args = arrayOf("file", 100))
            }
        }

        @Test
        fun defaultTimeout() = asyncTest {
            testZeroFrameCommandWithReturnValue("ok") whenAsync {
                obj.file.need("file")
            } then {
                assertZeroFrameCommandWithReturn("fileNeed", args = arrayOf("file", 300))
            }
        }

        @Test
        fun throwsOnFailure() = asyncTest {
            testZeroFrameCommandWithReturnValue("Failure") whenAsync {
                obj.file.need("file", 100)
            } then {
                assertNotNull(it.exception)
            }
        }
    }

    inner class Query {
        @Test
        fun passesProperParameters_toZeroFrame() = asyncTest {
            testZeroFrameCommandWithReturnValue(arrayOf(json("a" to 1, "b" to 83))) whenAsync {
                obj.file.query<Any>("query/path/*.json", "item")
            } then {
                assertZeroFrameCommandWithReturn("fileQuery", args = arrayOf("query/path/*.json", "item"))
            }
        }
    }
}