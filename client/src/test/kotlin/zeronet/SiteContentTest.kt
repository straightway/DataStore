package zeronet

import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals

class SiteContentTest {
    private val sut = json(
            "address" to "ValueOfaddress",
            "address_index" to 2,
            "clone_root" to "ValueOfclone_root",
            "cloned_from" to "ValueOfcloned_from",
            "description" to "ValueOfdescription",
            "files" to 3,
            "ignore" to "ValueOfignore",
            "includes" to 5,
            "inner_path" to "ValueOfinner_path",
            "modified" to 7,
            "postmessage_nonce_security" to true,
            "signs_required" to false,
            "title" to "ValueOftitle",
            "zeronet_version" to "ValueOfzeronet_version",
            "files_optional" to 11,
            "background-color" to "ValueOfbackground-color"
    ).unsafeCast<SiteContent>()

    @Test
    fun fieldsAreFilledAsExpected() {
        assertEquals("ValueOfaddress", sut.address)
        assertEquals(2, sut.addressIndex)
        assertEquals("ValueOfclone_root", sut.cloneRoot)
        assertEquals("ValueOfcloned_from", sut.clonedFrom)
        assertEquals("ValueOfdescription", sut.description)
        assertEquals(3, sut.files)
        assertEquals("ValueOfignore", sut.ignore)
        assertEquals(5, sut.includes)
        assertEquals("ValueOfinner_path", sut.innerPath)
        assertEquals(7, sut.modified)
        assertEquals(true, sut.postmessageNonceSecurity)
        assertEquals(false, sut.signsRequired)
        assertEquals("ValueOftitle", sut.title)
        assertEquals("ValueOfzeronet_version", sut.zeronetVersion)
        assertEquals(11, sut.filesOptional)
        assertEquals("ValueOfbackground-color", sut.backgroundColor)
    }
}