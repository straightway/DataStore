package zeronet

import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertEquals

class SiteSettingsTest {
    private val sut = json(
            "added" to 2,
            "ajax_key" to "ValueOfajax_key",
            "bytes_recv" to 3,
            "bytes_sent" to 5,
            "cache" to json("time_modified_files_check" to 3.14),
            "downloaded" to 7,
            "modified" to 11,
            "optional_downloaded" to 13,
            "own" to true,
            "peers" to 17,
            "permissions" to arrayOf("a", "b", "c"),
            "serving" to false,
            "size" to 19,
            "size_files_optional" to 23,
            "size_limit" to 29,
            "size_optional" to 31
    ).unsafeCast<SiteSettings>()

    @Test
    fun fieldsAreFilledAsExpected() {
        assertEquals(2, sut.added)
        assertEquals("ValueOfajax_key", sut.ajaxKey)
        assertEquals(3, sut.bytesRecv)
        assertEquals(5, sut.bytesSent)
        assertEquals(3.14, sut.cache.timeModifiedFilesCheck)
        assertEquals(7, sut.downloaded)
        assertEquals(11, sut.modified)
        assertEquals(13, sut.optionalDownloaded)
        assertEquals(true, sut.own)
        assertEquals(17, sut.peers)
        assertEquals(listOf("a", "b", "c"), sut.permissions.toList())
        assertEquals(false, sut.serving)
        assertEquals(19, sut.size)
        assertEquals(23, sut.sizeFilesOptional)
        assertEquals(29, sut.sizeLimit)
        assertEquals(31, sut.sizeOptional)
    }
}