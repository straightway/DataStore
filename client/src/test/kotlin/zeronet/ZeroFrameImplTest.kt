package zeronet

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import straightway.testing.js.*
import kotlin.js.json
import kotlin.test.*

class ZeroFrameImplTest : TestBase() {

    // region Private fields

    private lateinit var _sut: ZeroFrameImpl
    private lateinit var _testContext: ZeroFrameTestContext
    private var _responseReceived: Boolean = false

    // endregion

    // region Setup / tear down

    override fun setUp() {
        _responseReceived = false

        _testContext = ZeroFrameTestContext(
                object : Launcher {
                    override fun launch(block: suspend () -> Unit) {
                        GlobalScope.launch {
                            try {
                                block()
                            } catch (x: Exception) {
                                exception(x)
                            }
                        }
                    }
                })

        _sut = ZeroFrameImpl(_testContext.context)
    }

    // endregion

    @Test
    fun constructor_addsEventListener() = asyncTest {
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::addEventListener)("message", any) wasCalled atLeastOnce
        }
    }

    @Test
    fun constructor_setsApplicationReady() = asyncTest {
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it["cmd"] == "innerReady" }) wasCalled atLeastOnce
        }
    }

    @Test
    fun cmd_withoutParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        _testContext.messageReaction = checkMessageCallAndReturn("CMD", null, 83)
        val result = _sut.cmd<Int>("CMD")
        assertEquals(83, result)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.params == null }) wasCalled atLeastOnce
        }
    }

    @Test
    fun cmdNoResult_withoutParameters_issuesProperCommand_andGetsResponse() = asyncTest {
        _testContext.messageReaction = checkMessageCall("CMD", null)
        _sut.cmd<Unit>("CMD")
        assertTrue(_responseReceived)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.params == null }) wasCalled atLeastOnce
        }
    }

    @Test
    fun cmd_withPairAsSingleParameter_issuesCommand_withJsonParameter() = asyncTest {
        _testContext.messageReaction = checkMessageCall("CMD", json("pi" to 3.14))
        _sut.cmd<Unit>("CMD", "pi" to 3.14)
        assertTrue(_responseReceived)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.params?.pi == 3.14 }) wasCalled atLeastOnce
        }
    }

    @Test
    fun fireAndForget_withoutParameters_issuesProperCommand_andGetsResponse() {
        _sut.fireAndForget("CMD")
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.cmd == "CMD" && it.params == null }) wasCalled atLeastOnce
        }
    }

    @Test
    fun fireAndForget_withJsonParameter_issuesCommand_withJsonParameter() {
        _sut.fireAndForget("CMD", json("pi" to 3.14))
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.cmd == "CMD" && it.params?.pi == 3.14 }) wasCalled atLeastOnce
        }
    }

    @Test
    fun fireAndForget_withPairAsSingleParameter_issuesCommand_withJsonParameter()  {
        _sut.fireAndForget("CMD", "pi" to 3.14)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it.cmd == "CMD" && it.params?.pi == 3.14 }) wasCalled atLeastOnce
        }
    }

    @Test
    fun openWebSocked_isFiredWhenWebSocked_isOpenend() = asyncTest {
        var calls = 0
        _sut.openWebSocket.attach { ++calls }
        _testContext.receiveBackendMessageAsync("wrapperOpenedWebsocket")
        assertEquals(1, calls)
    }

    @Test
    fun closeWebSocked__isFiredWhenWebSocked_isClosed() = asyncTest {
        var calls = 0
        _sut.closeWebsocket.attach { ++calls }
        _testContext.receiveBackendMessageAsync("wrapperClosedWebsocket")
        assertEquals(1, calls)
    }

    @Test
    fun setSiteInfo_isForwardedTo_setSiteInfo_event() = asyncTest {
        val calls = mutableListOf<SiteInfoEvent>()
        _sut.setSiteInfo.attach { calls.add(it) }
        val evt = json("event" to arrayOf("Lalala"))
        _testContext.receiveBackendMessageAsync("setSiteInfo", params = evt)
        assertEquals("Lalala", calls.single().event!!.single())
    }

    @Test
    fun otherRequests_areForwardedTo_request_event() = asyncTest {
        val calls = mutableListOf<Message>()
        _sut.request.attach { calls.add(it) }
        _testContext.receiveBackendMessageAsync("otherRequest", params = "Hello")
        assertEquals("Hello", calls.single().params)
    }

    @Test
    fun response_withoutRequest_isIgnored() = asyncTest {
        _testContext.receiveBackendMessageAsync("response")
    }

    @Test
    fun response_receivedSecondTime_isIgnored() = asyncTest {
        _testContext.messageReaction = _testContext.respond(83) {
            _testContext.receiveBackendMessage("response", id = it.id + 1, to = it.id, returnValue = 83)
        }

        _sut.cmd<Int>("CMD")
    }

    @Test
    fun wrapperReady_isRespondedWith_innerReady() = asyncTest {
        _testContext.webSocketProviderMock.reset()
        _testContext.generateMessageEventFromServer(83, "wrapperReady")
        delay(1L)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs { it?.cmd == "innerReady" }) wasCalled atLeastOnce
        }
    }

    @Test
    fun ping_isAnsweredWith_pong() = asyncTest {
        _testContext.generateMessageEventFromServer(83, "ping")
        delay(1L)
        _testContext.webSocketProviderMock.verify {
            (WebSocketProvider::postMessage)(itIs {
                it?.cmd == "response" && it.to == 83 && it.result == "pong"
            }) wasCalled atLeastOnce
        }
    }

    @Test
    fun wrapperNonce_isDerivedFromUrl() = asyncTest  {
        val postMessageCall = _testContext.webSocketProviderMock.calls.first { it.funcName == "postMessage" }
        val wrapperNonce: String = postMessageCall.params.single()?.wrapper_nonce
        assertEquals(ZeroFrameTestContext.WRAPPER_NONCE, wrapperNonce)

    }

    // region Private

    private fun checkMessageCall(cmd: String, params: Any?): (dynamic) -> Unit  =
            checkMessageCallAndReturn(cmd, params, Unit)

    private fun checkMessageCallAndReturn(cmd: String, params: Any?, returnValue: kotlin.Any): (dynamic) -> Unit {
        return _testContext.respond(returnValue) {
            assertEquals(it.cmd, cmd)
            when (params)
            {
                null -> assertNull(it.params)
                is Iterable<*> -> assertEquals(params, it.paramsList)
                else -> assertEquals(params.toString(), it.params.toString())
            }

            assertNull(it.result)
            assertEquals(0, it.to)
            assertEquals(ZeroFrameTestContext.WRAPPER_NONCE, it.wrapperNonce)
            _responseReceived = true
        }
    }

    private val Message.paramsList get() = (params as Array<*>).toList()

    // endregion
}
