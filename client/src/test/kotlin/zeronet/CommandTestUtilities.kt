package zeronet

import straightway.testing.bdd.Given
import straightway.testing.bdd.GivenWhen
import straightway.testing.js.*
import kotlin.js.json

fun testZeroFrameCommandWithReturnValue(commandResult: Any?) =
        Given {
            ZeroFrameMock().apply { obj.setCommandResult(commandResult) }
        }

fun GivenWhen<Mock<ZeroFrameMock>, *>.thenHasParameters(cmd: String, parameterChecker: (dynamic) -> Boolean) =
        then {
            assertZeroFrameCommandWithReturn(cmd, parameterChecker = parameterChecker)
        }

fun Mock<ZeroFrameMock>.verifyCommandFireAndForget(commandID: String, vararg args: Any?) =
        verifySequence { +wasCommandFiredWith(commandID, *args) }

fun Mock<ZeroFrameMock>.verifyCommandCall(commandID: String, vararg args: Pair<String, Any?>) =
        verifySequence { +wasCommandCalledWith(commandID, json(*args)) }

fun Mock<ZeroFrameMock>.verifyCommandCall(commandID: String, vararg args: Any?) =
        verifySequence { +wasCommandCalledWith(commandID, *args) }

fun Mock<ZeroFrameMock>.verifyZeroFrameCommandFireAndForget(
        commandID: String,
        args: Any? = any,
        parameterChecker: (dynamic) -> Boolean = { true }) =
        verifySequence { +wasZeroFrameCommandFireAndForget(commandID, args, parameterChecker) }

fun Mock<ZeroFrameMock>.assertZeroFrameCommandWithReturn(
        commandID: String,
        args: Any? = any,
        result: Any? = any,
        parameterChecker: (dynamic) -> Boolean = { true }) =
        verifySequence { +wasZeroFrameCommandCallWithReturn(commandID, args, result, parameterChecker) }

fun Mock<ZeroFrameMock>.wasCommandFiredWith(commandID: String) =
        wasZeroFrameCommandFireAndForget(commandID)

fun Mock<ZeroFrameMock>.wasCommandFiredWith(commandID: String, vararg args: Any?) =
        wasZeroFrameCommandFireAndForget(
                commandID,
                args = if (args.any()) checkArguments(args) else null)

fun Mock<ZeroFrameMock>.wasCommandFiredWith(commandID: String, vararg args: Pair<String, Any?>) =
        wasZeroFrameCommandFireAndForget(
                commandID,
                args = if (args.any()) checkArguments(arrayOf(json(*args))) else null)

fun Mock<ZeroFrameMock>.wasCommandCalledWith(commandID: String) =
        wasZeroFrameCommandCallWithReturn(commandID)

fun Mock<ZeroFrameMock>.wasCommandCalledWith(commandID: String, vararg args: Any?) =
        wasZeroFrameCommandCallWithReturn(
                commandID,
                args = if (args.any()) checkArguments(args) else null)

fun Mock<ZeroFrameMock>.wasCommandCalledWith(commandID: String, vararg args: Pair<String, Any?>) =
        wasZeroFrameCommandCallWithReturn(
                commandID,
                args = if (args.any()) checkArguments(arrayOf(json(*args))) else null)

fun Mock<ZeroFrameMock>.wasZeroFrameCommandFireAndForget(
        commandID: String,
        args: Any? = any,
        parameterChecker: (dynamic) -> Boolean = { true }
): ExpectedCall {
    val stringRep = "$commandID(${args.prettyString})"
    return ExpectedCall(this, stringRep) { call ->
        val checkResult = call.isCallTo("fireAndForget", commandID, args)
        if (true == checkResult) parameterChecker(call.params[1])
        else checkResult
    }
}

fun Mock<ZeroFrameMock>.wasZeroFrameCommandCallWithReturn(
        commandID: String,
        args: Any? = any,
        expectedResult: Any? = any,
        parameterChecker: (dynamic) -> Boolean = { true }
): ExpectedCall {
    val stringRep = "$commandID(${args.prettyString}) = ${expectedResult.prettyString}"
    return ExpectedCall(this, stringRep) { call ->
        val commandResult = obj.getCommandResult(commandID)
        if (!expectedResult.matches(commandResult))
                "Expected result: $expectedResult, actual: $commandResult"
        else call.isCallTo("cmd", commandID, args).let {
            if (it == true) parameterChecker(call.params[1]) else it
        }
    }
}

private fun Any?.matches(actual: Any?) =
        when {
            this === null -> actual === null
            this is MockConstraint -> matches(actual)
            else -> equals(actual)
        }

private fun checkArguments(args: Array<out Any?>): Any =
        itIs<Any?>(args.prettyString) { arg ->
            if (arg is Array<*>) {
                (JSON.stringify(args) == JSON.stringify(arg) ||
                arg.size == args.size &&
                arg.mapIndexed { index, item -> args[index].isMatching(item) }.all { it }).also {
                    explanation += ", invalid array content"
                }
            } else {
                (args.size == 1 && args.single().isMatching(arg)).also {
                    explanation += ", invalid single argument"
                }
            }
        }

private val Any?.prettyString: String get() =
    when (this) {
        null -> "<null>"
        is MockConstraint -> toString()
        is Iterable<*> -> this.joinToString(", ") { it.prettyString }
        is Array<*> -> this.joinToString(", ") { it.prettyString }
        else -> JSON.stringify(this)
    }

private fun Any?.isMatching(item: Any?): Boolean =
        when (this) {
            is MockConstraint -> matches(item)
            else -> this == item || JSON.stringify(this) == JSON.stringify(item)
        }
