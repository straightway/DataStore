package zeronet

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ServerPortOpenedTest {
    @Test
    fun serverPortOpened_IsMarshalledProperly() {
        val sut = js("{\"ipv4\":true,\"ipv6\":false}").unsafeCast<ServerPortOpened>()
        assertTrue(sut.ipv4)
        assertFalse(sut.ipv6)
    }
}