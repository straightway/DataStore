package zeronet

import straightway.testing.js.call
import straightway.testing.js.Mock
import straightway.testing.js.MockSetup
import straightway.utils.AsyncEvent
import kotlin.test.fail

class ZeroFrameMock(val mockSetup: MockSetup) : ZeroFrame {
    companion object {
        operator fun invoke() = Mock { ZeroFrameMock(this) }
    }

    private var _generalCommandResult: Any? = null
    private val _specialCommandResults = mutableMapOf<String, Any?>()
    val openWebSocketEvent = AsyncEvent<Unit>()
    val closeWebSocketEvent = AsyncEvent<Unit>()
    val setSiteInfoEvent = AsyncEvent<SiteInfoEvent>()
    val requestEvent = AsyncEvent<Message>()
    fun getCommandResult(cmd: String) = _specialCommandResults[cmd] ?: _generalCommandResult
    fun setCommandResult(result: Any?) { _generalCommandResult = result }
    fun setCommandResult(cmd: String, result: Any?) { _specialCommandResults[cmd] = result }
    override val openWebSocket = openWebSocketEvent
    override val closeWebsocket = closeWebSocketEvent
    override val setSiteInfo = setSiteInfoEvent
    override val request = requestEvent
    val loggerMock = Mock<Logger> {
        object : Logger {
            override fun log(l: Logger.Level, component: String, s: String) =
                    call(Logger::log, l, component, s)
        }
    }
    override val logger = loggerMock.obj
    override suspend fun <T> cmd(cmd: String, params: Any?): T =
            mockSetup.call("cmd", cmd, params) {
                if (!isCommandSetUp(cmd)) fail("Command $cmd not set up")
                try {
                    @Suppress("UNCHECKED_CAST")
                    getCommandResult(cmd) as T
                } catch (x: Throwable) {
                    fail("Exception during $cmd call: $x")
                }
            }

    override fun fireAndForget(cmd: String, params: Any?) {
        mockSetup.call(ZeroFrame::fireAndForget, cmd, params)
    }

    // region private

    private fun isCommandSetUp(cmd: String) =
            cmd in _specialCommandResults || _generalCommandResult != null

    // endregion
}