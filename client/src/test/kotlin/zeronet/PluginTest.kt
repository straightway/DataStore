package zeronet

import straightway.testing.bdd.Given
import straightway.testing.js.TestBase
import kotlin.test.Test
import kotlin.test.assertNotNull

class PluginTest : TestBase() {
    private val test get() = Given {
        ZeroFrameMock().apply() {
            obj.setCommandResult("ok")
        }
    }

    inner class addRequest {
        @Test
        fun makesProperZeroFrameCall() = asyncTest {
            test whenAsync {
                obj.plugin.addRequest("path/to/plugin")
            } then {
                assertZeroFrameCommandWithReturn(
                        "pluginAddRequest",
                        args = "path/to/plugin")
            }
        }

        @Test
        fun throwsOnError() = asyncTest {
            test while_ {
                obj.setCommandResult("Error")
            } whenAsync {
                obj.plugin.addRequest("path/to/plugin")
            } then {
                assertNotNull(it.exception, "Expected exception not thrown")
            }
        }
    }
}