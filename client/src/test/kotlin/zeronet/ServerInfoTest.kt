package zeronet

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class ServerInfoTest {
    private companion object {
        val SERVER_INFO = js(
                "{" +
                    "\"ip_external\":false," +
                    "\"port_opened\":{\"ipv4\":false,\"ipv6\":false}," +
                    "\"platform\":\"linux\"," +
                    "\"fileserver_ip\":\"127.0.0.1\"," +
                    "\"fileserver_port\":15442," +
                    "\"tor_enabled\":true," +
                    "\"tor_status\":\"OK (10 onions running)\"," +
                    "\"tor_has_meek_bridges\":false," +
                    "\"tor_use_bridges\":false," +
                    "\"ui_ip\":\"127.0.0.1\"," +
                    "\"ui_port\":43110," +
                    "\"version\":\"0.7.1\"," +
                    "\"rev\":4206," +
                    "\"timecorrection\":-27.428980668385822," +
                    "\"language\":\"de\"," +
                    "\"debug\":false," +
                    "\"offline\":false," +
                    "\"plugins\":[\"AnnounceBitTorrent\",\"AnnounceLocal\",\"AnnounceShare\",\"AnnounceZero\",\"Bigfile\",\"Chart\",\"ContentFilter\",\"Cors\",\"CryptMessage\",\"FilePack\",\"MergerSite\",\"Newsfeed\",\"OptionalManager\",\"PeerDb\",\"Sidebar\",\"Stats\",\"TranslateSite\",\"Trayicon\",\"UiConfig\",\"UiPluginManager\",\"Zeroname\",\"Straightway\"]," +
                    "\"plugins_rev\":{\"Straightway\":1}," +
                    "\"user_settings\":{\"Setting\":13}" +
                "}")
    }

    @Test
    fun serverInfo_IsMarshalledProperly() {
        val sut = SERVER_INFO.unsafeCast<ServerInfo>()
        assertFalse(sut.ipExternal)
        assertFalse(sut.portOpened.ipv4)
        assertFalse(sut.portOpened.ipv6)
        assertEquals("linux", sut.platform)
        assertEquals("127.0.0.1", sut.fileserverIp)
        assertEquals(15442, sut.fileServerPort)
        assertTrue(sut.torEnabled)
        assertEquals("OK (10 onions running)", sut.torStatus)
        assertFalse(sut.torHasMeekBridges)
        assertFalse(sut.torUseBridges)
        assertEquals("127.0.0.1", sut.uiIp)
        assertEquals(43110, sut.uiPort)
        assertEquals("0.7.1", sut.version)
        assertEquals(4206, sut.rev)
        assertEquals(-27.428980668385822, sut.timeCorrection)
        assertEquals("de", sut.language)
        assertFalse(sut.debug)
        assertFalse(sut.offline)
        assertEquals(
                listOf("AnnounceBitTorrent", "AnnounceLocal", "AnnounceShare", "AnnounceZero", "Bigfile", "Chart", "ContentFilter", "Cors", "CryptMessage", "FilePack", "MergerSite", "Newsfeed", "OptionalManager", "PeerDb", "Sidebar", "Stats", "TranslateSite", "Trayicon", "UiConfig", "UiPluginManager", "Zeroname", "Straightway"),
                sut.plugins.toList())
        assertEquals(1, sut.pluginsRev["Straightway"])
        assertEquals(13, sut.userSettings["Setting"])
    }
}