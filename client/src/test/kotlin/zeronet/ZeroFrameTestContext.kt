package zeronet

import kotlinx.coroutines.delay
import straightway.testing.js.ArgWithString
import straightway.testing.js.call
import straightway.testing.js.Mock
import kotlin.js.json

class ZeroFrameTestContext(launcher: Launcher) {

    private val _eventListeners = mutableMapOf<String, (dynamic) -> Unit>()

    companion object {
        const val WRAPPER_NONCE = "WRAPPERNONCE"
    }

    val webSocketProviderMock: Mock<WebSocketProvider> = Mock {
        object : WebSocketProvider {
            override fun postMessage(message: Any?) {
                call(WebSocketProvider::postMessage, ArgWithString(message, JSON.stringify(message)))
                messageReaction(message)
            }

            override fun addEventListener(type: String, callback: ((dynamic) -> Unit)?) {
                call(WebSocketProvider::addEventListener, type, callback)
                if (callback != null) _eventListeners[type] = callback
            }
        }
    }

    val browserMock: Mock<Browser> = Mock {
        object : Browser {
            override val document: MinDocument = object : MinDocument {
                override val location: dynamic =
                        json("href" to "https://a.b.cd?wrapper_nonce=$WRAPPER_NONCE/")
            }
        }
    }

    val loggerMock: Mock<Logger> = Mock {
        object : Logger {
            override fun log(l: Logger.Level, component: String, s: String) = call(Logger::log, l, component, s)
        }
    }

    val context = ZeroFrameContext(
            webSocketProviderMock.obj,
            browserMock.obj,
            launcher,
            loggerMock.obj)

    val eventListeners: Map<String, (dynamic) -> Unit> get() = _eventListeners

    var messageReaction: (msg: dynamic) -> Unit = {}
}

fun ZeroFrameTestContext.generateMessageEventFromServer(id: Int, cmd: String, params: Any? = null, result: Any? = null) {
    val message = json(
            "cmd" to cmd,
            "to" to 0,
            "params" to params,
            "result" to result,
            "id" to id,
            "wrapper_nonce" to ZeroFrameTestContext.WRAPPER_NONCE)
    val messageEvent = json("data" to message)
    eventListeners["message"]!!(messageEvent)

}

@Suppress("UnsafeCastFromDynamic")
fun ZeroFrameTestContext.respond(returnValue: Any?, messageAsserter: (Message) -> Unit = {}): (dynamic) -> Unit = {
    messageAsserter(it)
    receiveBackendMessage("response", id = it.id + 1, to = it.id, returnValue = returnValue)
}

suspend fun ZeroFrameTestContext.receiveBackendMessageAsync(
        cmd: String,
        id: Int = 0,
        to: Int = 0,
        returnValue: Any? = null,
        params: Any? = null
) {
    receiveBackendMessage(cmd, id, to, returnValue, params)
    delay(1L)
}

@Suppress("UnsafeCastFromDynamic")
fun ZeroFrameTestContext.receiveBackendMessage(
        cmd: String,
        id: Int = 0,
        to: Int = 0,
        returnValue: Any? = null,
        params: Any? = null
) {
    val response: dynamic = json(
            "cmd" to cmd,
            "params" to params,
            "to" to to,
            "result" to returnValue,
            "id" to id,
            "wrapper_none" to "Lalala")
    val event: dynamic = json("data" to response)
    eventListeners["message"]?.invoke(event)
}
