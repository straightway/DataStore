package zeronet

import straightway.testing.js.TestBase
import kotlin.js.json
import kotlin.test.Test

class InteractionTest : TestBase() {

    @Test
    fun notify_passesProperParameters_toZeroFrame() =
            testZeroFrameCommandWithReturnValue(Unit) when_ {
                obj.interaction.notify(Interaction.NotificationType.Info, "Message", 3000)
            } then {
                verifyZeroFrameCommandFireAndForget(
                        "wrapperNotification",
                        args = arrayOf("info", "Message", 3000))
            }

    @Test
    fun notify_withoutTimeout_omitsTimeoutInCmdCall() = asyncTest {
        testZeroFrameCommandWithReturnValue(Unit) whenAsync {
            obj.interaction.notify(Interaction.NotificationType.Info, "Message")
        } then {
            verifyZeroFrameCommandFireAndForget(
                    "wrapperNotification",
                    args = arrayOf("info", "Message"))
        }
    }

    @Test
    fun notifyInfo_passesProperParameters_toZeroFrame() =
            testZeroFrameCommandWithReturnValue(Unit) when_ {
                obj.interaction.notifyInfo("Message", 3000)
            } then {
                verifyZeroFrameCommandFireAndForget(
                        "wrapperNotification",
                        args = arrayOf("info", "Message", 3000))
            }

    @Test
    fun notifyError_passesProperParameters_toZeroFrame() =
            testZeroFrameCommandWithReturnValue(Unit) when_ {
                obj.interaction.notifyError("Message", 3000)
            } then {
                verifyZeroFrameCommandFireAndForget(
                        "wrapperNotification",
                        args = arrayOf("error", "Message", 3000))
            }

    @Test
    fun notifyDone_passesProperParameters_toZeroFrame() =
            testZeroFrameCommandWithReturnValue(Unit) when_ {
                obj.interaction.notifyDone("Message", 3000)
            } then {
                verifyZeroFrameCommandFireAndForget(
                        "wrapperNotification",
                        args = arrayOf("done", "Message", 3000))
            }

    @Test
    fun certSelect_passesProperParameters_toZeroFrame() = asyncTest {
        testZeroFrameCommandWithReturnValue(Unit) whenAsync {
            obj.interaction.certSelect("D1", "D2", acceptAny = true, acceptedPattern = ".*")
        } then {
            assertZeroFrameCommandWithReturn(
                    "certSelect",
                    args = json(
                            "accepted_domains" to arrayOf("D1", "D2"),
                            "accept_any" to true,
                            "accepted_pattern" to ".*"
                    ))
        }
    }

    @Test
    fun certSelect_withoutSelectedDomains_passesAcceptedDomains_EmptyArray() = asyncTest {
        testZeroFrameCommandWithReturnValue(Unit).whenAsync {
            obj.interaction.certSelect()
        }.thenHasParameters("certSelect") {
            it["accepted_domains"].length == 0
        }
    }

    @Test
    fun certSelect_withoutSelectedDomains_passesAcceptAny_false() = asyncTest {
        testZeroFrameCommandWithReturnValue(Unit).whenAsync {
            obj.interaction.certSelect()
        }.thenHasParameters("certSelect") {
            it["accept_any"] == false
        }
    }

    @Test
    fun certSelect_withoutSelectedDomains_omitsAcceptedPattern() = asyncTest {
        testZeroFrameCommandWithReturnValue(Unit).whenAsync {
            obj.interaction.certSelect()
        }.thenHasParameters("certSelect") {
            it.hasOwnProperty("accepted_pattern") == false
        }
    }
}