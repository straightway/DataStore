package zeronet

import straightway.testing.bdd.Given
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class MessageTest {

    @Test
    fun cmd_isAsSpecified() =
            Given {
                Message(cmd = "CMD" )
            } when_ {
                cmd
            } then {
                assertEquals("CMD", it.result)
            }

    @Test
    fun to_isZeroByDefault() =
            Given {
                Message(cmd = "CMD")
            } when_ {
                to
            } then {
                assertEquals(0, it.result)
            }

    @Test
    fun to_isAsSpecified() =
            Given {
                Message(cmd = "CMD", to = 83)
            } when_ {
                to
            } then {
                assertEquals(83, it.result)
            }

    @Test
    fun params_isNullByDefault() =
            Given {
                Message(cmd = "CMD")
            } when_ {
                params
            } then {
                assertNull(it.nullableResult)
            }

    @Test
    fun params_isAsSpecified() =
            Given {
                Message(cmd = "CMD", params = 83)
            } when_ {
                params
            } then {
                assertEquals(83, it.result)
            }

    @Test
    fun result_isNullByDefault() =
            Given {
                Message(cmd = "CMD")
            } when_ {
                result
            } then {
                assertNull(it.nullableResult)
            }

    @Test
    fun result_isAsSpecified() =
            Given {
                Message(cmd = "CMD", result = 83)
            } when_ {
                result
            } then {
                assertEquals(83, it.result)
            }

    @Test
    fun wrapperNonce_isEmptyByDefault() =
            Given {
                Message(cmd = "CMD")
            } when_ {
                wrapperNonce
            } then {
                assertEquals("", it.result)
            }

    @Test
    fun wrapperNonde_isAsSpecified() =
            Given {
                Message(cmd = "CMD", wrapperNonce = "Hello")
            } when_ {
                wrapperNonce
            } then {
                assertEquals("Hello", it.result)
            }

    @Test
    fun id_isGloballyCountedUp() =
            Given {
                listOf(Message("CMD"), Message("CMD"))
            } when_ {
                map { it.id }.sorted()
            } then {
                assertTrue(it.result.first() < it.result.last())
            }
}