package zeronet

import straightway.testing.js.TestBase
import kotlin.js.json
import kotlin.test.Test
import kotlin.test.assertFails

class SiteTest : TestBase() {

    @Test
    fun getInfo_passesProperParameters_toZeroFrame() = asyncTest {
        val result = json("auth_address" to "1234").unsafeCast<SiteInfo>()
        testZeroFrameCommandWithReturnValue(result) whenAsync {
            obj.site.getInfo()
        } then {
            assertZeroFrameCommandWithReturn("siteInfo", args = null, result = it.result)
        }
    }

    @Test
    fun sign_passesProperParameters_toZeroFrame() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok") whenAsync {
            obj.site.sign(privateKey = "PK", innerPath = "IP", removeMissionOptions = true)
        } then {
            assertZeroFrameCommandWithReturn(
                    "siteSign",
                    args = json(
                            "remove_missing_optional" to true,
                            "privatekey" to "PK",
                            "inner_path" to "IP"
                    ))
        }
    }

    @Test
    fun sign_throwsOnFailure() = asyncTest {
        testZeroFrameCommandWithReturnValue("Failure") whenAsync {
            obj.site.sign()
        } then {
            assertFails { it.nullableResult }
        }
    }

    @Test
    fun sign_defaultValue_removeMissionOptions_isFalse() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.sign()
        }.thenHasParameters("siteSign") {
            it["remove_missing_optional"] == false
        }
    }

    @Test
    fun sign_omitPrivateKey_isMissing() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.sign()
        }.thenHasParameters("siteSign") {
            it.hasOwnProperty("privatekey") == false
        }
    }

    @Test
    fun sign_omitInnerPass_isMissing() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.sign()
        }.thenHasParameters("siteSign") {
            it.hasOwnProperty("inner_path") == false
        }
    }

    @Test
    fun publish_passesProperParameters_toZeroFrame() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok") whenAsync {
            obj.site.publish(privateKey = "PK", innerPath = "IP", sign = false)
        } then {
            assertZeroFrameCommandWithReturn(
                    "sitePublish",
                    args = json(
                            "sign" to false,
                            "privatekey" to "PK",
                            "inner_path" to "IP"
                    ))
        }
    }

    @Test
    fun publish_throwsOnFailure() = asyncTest {
        testZeroFrameCommandWithReturnValue("Failure") whenAsync {
            obj.site.publish()
        } then {
            assertFails { it.nullableResult }
        }
    }

    @Test
    fun publish_defaultValue_sign_isTrue() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.publish()
        }.thenHasParameters("sitePublish") {
            it["sign"] == true
        }
    }

    @Test
    fun publish_omitPrivateKey_isMissing() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.publish()
        }.thenHasParameters("sitePublish") {
            it.hasOwnProperty("privatekey") == false
        }
    }

    @Test
    fun publish_omitInnerPass_isMissing() = asyncTest {
        testZeroFrameCommandWithReturnValue("ok").whenAsync {
            obj.site.publish()
        }.thenHasParameters("sitePublish") {
            it.hasOwnProperty("inner_path") == false
        }
    }

    @Test
    fun getServerInfo_passesProperParameters_toZeroFrame() = asyncTest {
        val result = json("ip_external" to "1.2.3.4").unsafeCast<ServerInfo>()
        testZeroFrameCommandWithReturnValue(result) whenAsync {
            obj.site.getServerInfo()
        } then {
            assertZeroFrameCommandWithReturn("serverInfo", args = null, result = it.result)
        }
    }
}