package straightway.datastore.ui

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.asPromise
import kotlinx.coroutines.async
import org.w3c.dom.Document
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLInputElement
import straightway.datastore.DataStoreUI
import straightway.utils.AsyncEvent

class DataStoreUIImpl(document: Document): DataStoreUI {

    // region Private fields
    private val messagesElement = document.getElementById("messages").unsafeCast<HTMLElement>()
    private val commandInputElement = document.getElementById("command").unsafeCast<HTMLInputElement>()
    // endregion

    // region DataStoreUI

    override val issueCommandEvent = AsyncEvent<String>()
    override val deleteMessageEvent = AsyncEvent<String>()
    override val rePublishEvent = AsyncEvent<String>()
    override fun clearMessageInput() { commandInputElement.value = "" }
    override fun addMessage(
            id: String,
            messageText: String
    ) {
        val messageEscaped = messageText
                .replace("<", "&lt;")
                .replace(">", "&gt;")  // Escape html tags in the message
        if (id.isEmpty())
            messagesElement.innerHTML = "<li>$messageEscaped</li>" + messagesElement.innerHTML
        else
            messagesElement.innerHTML +=
                    "<li>" +
                            "$id " +
                            "<button onclick=\"page.onDelete('$id')\">DEL</button> " +
                            "<button onclick=\"page.onRePublish('$id')\">REPUB</button> " +
                            "$messageEscaped " +

                    "</li>"
    }
    override fun clearMessages() { messagesElement.innerHTML = "" }

    // endregion

    // region Callbacks

    @Suppress("unused")
    fun issueCommand() =
            GlobalScope.async { issueCommandEvent(commandInputElement.value) }.asPromise()

    @JsName("onDelete")
    @Suppress("unused")
    fun onDelete(directory: String)  =
            GlobalScope.async { deleteMessageEvent(directory) }.asPromise()

    @JsName("onRePublish")
    @Suppress("unused")
    fun onRePublish(address: String)  =
            GlobalScope.async { rePublishEvent(address) }.asPromise()

    // endregion

    // region init

    init {
        document.asDynamic().page = this
    }

    // endregion
}