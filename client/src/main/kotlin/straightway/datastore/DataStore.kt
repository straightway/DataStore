package straightway.datastore

import straightway.error.Panic
import zeronet.*
import kotlin.js.JSON

class DataStore(
        zeroFrame: ZeroFrame = ZeroFrame(ZeroFrameContext()),
        private val ui: DataStoreUI
) : ZeroFrame by zeroFrame {

    // region Private properties

    private companion object {
        const val INNER_USER_PATH = "data/users"
        val commandRegex = Regex("([a-zA-Z0-9_]+)?\\s+(.*)")
        val configSetRegex = Regex("([a-zA-Z0-9_]+)\\s*=\\s*(.*)")
        val idRegex = Regex("(.*)/data.bin")
    }

    private lateinit var siteInfo: SiteInfo
    private var loadTimeout = 300

    // endregion

    //region Event handlers

    private suspend fun onOpenWebsocket() = doNotifyingExceptionsAsync {
        makeSurePluginVersion("plugins", "Straightway", 1)
        siteInfo = site.getInfo()
        displayAllMessages()
    }

    private suspend fun onSiteInfoSet(siteInfoEvent: SiteInfoEvent) = doNotifyingExceptionsAsync {
        siteInfo = siteInfoEvent
        if (siteInfoEvent.isFileDoneEvent)
            displayAllMessages()
    }

    private suspend fun onIssueCommand(cmd: String) = doNotifyingExceptionsAsync {
        ui.clearMessageInput()
        val cmdMatch = commandRegex.matchEntire(cmd)
        val command = cmdMatch?.groupValues?.get(1) ?: "store"
        val parameter = cmdMatch?.groupValues?.get(2) ?: cmd
        when (command)
        {
            "load" -> load(parameter)
            "store" -> anonymouslySend(parameter)
            "get" -> getConfigValue(parameter)
            "set" -> setConfigValue(parameter)
            "cmd" -> issueZeroNetCommand(parameter)
            else -> interaction.notifyError("Invalid command \"$command\" in: $cmd")
        }
    }

    private suspend fun onDeleteMessage(userId: String) {
        straightway.deleteLocally(getDirectory(userId))
        displayAllMessages()
    }

    private suspend fun onRePublishMessage(id: String) {
        site.publish(innerPath = getContentInnerPath(id), sign = false)
    }

    // endregion

    // region Private

    private suspend fun load(dataID: String) {
        file.need(getDataInnerPath(dataID), loadTimeout)
    }

    private suspend fun anonymouslySend(messageText: String) {
        val cert = straightway.create()
        straightway.setTemp(siteInfo.address, cert)

        try {
            addMessage(messageText, cert.authAddress)
            displayAllMessages()
            publishInnerContent(cert)
        }
        finally {
            straightway.setTemp(siteInfo.address, null)
        }
    }

    private fun getConfigValue(item: String) {
        val result = when (item) {
            "loadTimeout" -> loadTimeout.toString()
            else -> "<unknown>"
        }
        interaction.notifyInfo("$item=$result", 5000)
    }

    private fun setConfigValue(specification: String) {
        val cmdMatch = configSetRegex.matchEntire(specification) ?: throw Panic("Invalid config specification")
        val item = cmdMatch.groupValues[1]
        val value = cmdMatch.groupValues[2]
        when (item) {
            "loadTimeout" -> loadTimeout = value.toInt()
            else -> {
                interaction.notifyError("cannot set $item")
                return
            }
        }

        interaction.notifyInfo("$item=$value", 5000)
    }

    private suspend fun issueZeroNetCommand(cmdSpec: String) {
        ui.addMessage("", "cmd $cmdSpec")
        val cmdMatch = commandRegex.matchEntire(cmdSpec) ?: throw Panic("Invalid command")
        val command = cmdMatch.groupValues[1]
        val parameter = cmdMatch.groupValues[2]
        val result = JSON.stringify(cmd(command, JSON.parse(parameter)))
        val resultMsg = "$command -> $result"
        logger.info("DataStore", resultMsg)
        interaction.notifyInfo(resultMsg, 5000)
    }

    private suspend fun makeSurePluginVersion(pluginDir: String, pluginName: String, version: Int) {
        if (site.getServerInfo().pluginsRev[pluginName] != version)
            plugin.addRequest("$pluginDir/$pluginName")
    }

    private suspend fun publishInnerContent(cert: Cert) {
        try {
            val contentInnerPath = getContentInnerPath(cert.authAddress)
            site.sign(innerPath = contentInnerPath, privateKey = cert.authPrivateKey)
            site.publish(innerPath = contentInnerPath, privateKey = cert.authPrivateKey, sign = false)
        }
        catch (x: Panic) {
            interaction.notifyError("Publication error: " + JSON.stringify(x.state))
        }
    }

    private suspend fun addMessage(newMessage: String, authAddress: String) =
        file.write(getDataInnerPath(authAddress), btoa(newMessage))

    private suspend fun displayAllMessages() {
        val allDataFiles = file.list(INNER_USER_PATH).filter { it.endsWith("/data.bin") }
        ui.clearMessages()
        allDataFiles.forEach {
            val id = idRegex.matchEntire(it)?.groups?.get(1)?.value ?: "<null>"
            ui.addMessage(id, file.read(getDataInnerPath(id)))
        }
    }

    private fun getDirectory(authAddress: String) = "$INNER_USER_PATH/$authAddress"
    private fun getDataInnerPath(authAddress: String) = "${getDirectory(authAddress)}/data.bin"
    private fun getContentInnerPath(authAddress: String) = "${getDirectory(authAddress)}/content.json"
    private val SiteInfoEvent.isFileDoneEvent get() = event?.get(0) ?: "" == "file_done"

    // endregion

    // region Init

    init {
        with (zeroFrame) {
            openWebSocket.attach { onOpenWebsocket() }
            setSiteInfo.attach { onSiteInfoSet(it) }
        }

        with (ui) {
            issueCommandEvent.attach { onIssueCommand(it) }
            deleteMessageEvent.attach { onDeleteMessage(it) }
            rePublishEvent.attach { onRePublishMessage(it) }
        }
    }

    // endregion
}
