package straightway.datastore

import kotlin.js.Date
import kotlin.js.json

external interface ChatMessage {
    @JsName("body")
    val body: String

    @JsName("date_added")
    val dateAdded: Double

    @JsName("directory")
    val directory: String
}

fun ChatMessage(directory: String, messageText: String): ChatMessage =
    json(
            "body" to messageText,
            "date_added" to Date.now(),
            "directory" to directory
    ).unsafeCast<ChatMessage>()
