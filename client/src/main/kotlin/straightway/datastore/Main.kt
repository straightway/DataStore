package straightway.datastore

import straightway.datastore.ui.DataStoreUIImpl
import zeronet.Logger
import zeronet.ZeroFrame
import zeronet.ZeroFrameContext
import zeronet.info
import kotlin.browser.document

fun main() {
    if (isInBrowser()) {
        val logger = Logger(minLevel = Logger.Level.Debug)
        val context = ZeroFrameContext(logger = logger)
        val ui = DataStoreUIImpl(document)
        DataStore(ZeroFrame(context), ui)
        logger.info("DataStore", "Starting DataStore build $VERSION")
    }
}

private fun isInBrowser() =
        try {
            document
            true
        }
        catch (_: Throwable) {
            false
        }
