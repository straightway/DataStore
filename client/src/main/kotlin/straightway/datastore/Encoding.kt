package straightway.datastore

private val x = js("""
if (typeof btoa === 'undefined' || typeof atob === 'undefined') {
  global.Buffer = global.Buffer || require('buffer').Buffer;
    
    if (typeof btoa === 'undefined') {
        global.btoa = function (str) {
            return new Buffer.from(str, 'binary').toString('base64');
        };
    }
    
    if (typeof atob === 'undefined') {
        global.atob = function (b64Encoded) {
            return new Buffer.from(b64Encoded, 'base64').toString('binary');
        };
    }
}
""")

external fun encodeURIComponent(str: String): String
external fun unescape(str: String): String
external fun btoa(str: String): String
external fun atob(str: String): String
fun Any.toBase64() = btoa(unescape(encodeURIComponent(JSON.stringify(this, undefined, "\t"))))