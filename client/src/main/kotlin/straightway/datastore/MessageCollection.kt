package straightway.datastore

import kotlin.js.json

external interface MessageCollection {
    @JsName("message")
    val messages: MutableList<ChatMessage>
}

fun MessageCollection(vararg contents: ChatMessage) =
        json("message" to contents.toMutableList()).unsafeCast<MessageCollection>()