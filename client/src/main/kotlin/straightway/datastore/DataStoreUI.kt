package straightway.datastore

import straightway.utils.AsyncEventRegistry

interface DataStoreUI {
    val issueCommandEvent: AsyncEventRegistry<String>
    val deleteMessageEvent: AsyncEventRegistry<String>
    val rePublishEvent: AsyncEventRegistry<String>

    fun clearMessageInput()

    fun addMessage(id: String, messageText: String)
    fun clearMessages()
}