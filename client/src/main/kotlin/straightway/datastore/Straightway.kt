package straightway.datastore

import zeronet.*

interface Straightway {
    suspend fun create(): Cert
    suspend fun setTemp(siteAddress: String, cert: Cert?)
    suspend fun deleteLocally(innerPath: String)
}

val ZeroFrame.straightway: Straightway get() =
        object : Straightway {
            override suspend fun create(): Cert {
                val result = cmd<String>("createSelfSignedIdentity")
                return JSON.parse(atob(result))
            }

            override suspend fun setTemp(siteAddress: String, cert: Cert?) {
                val encodedCert =  if (cert == null) null else btoa(JSON.stringify(cert))
                cmd<Unit>("setTempCert", siteAddress, encodedCert)
            }

            override suspend fun deleteLocally(innerPath: String) {
                cmdThrow("deleteLocally", innerPath)
            }
        }