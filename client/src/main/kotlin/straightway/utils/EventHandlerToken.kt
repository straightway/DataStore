package straightway.utils

/**
 * This token is returned by event attach to identify the connection for a
 * later detach.
 */
class EventHandlerToken