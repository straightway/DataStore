package zeronet

data class ZeroFrameContext(
        val target: WebSocketProvider = WebSocketProvider(),
        val browser: Browser = Browser(),
        val launcher: Launcher = Launcher(),
        val logger: Logger = Logger())