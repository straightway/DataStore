package zeronet

fun notNullNamedParameters(vararg params: Pair<String, Any?>): Array<Pair<String, Any>> =
        params.filter { it.second != null }.map { it.first to it.second!! }.toTypedArray()

