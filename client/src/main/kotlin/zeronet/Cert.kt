package zeronet

import kotlin.js.json

external interface Cert {
    @JsName("auth_user_name")
    val authUserName: String

    @JsName("auth_type")
    val authType: String


    @JsName("auth_privatekey")
    val authPrivateKey: String

    @JsName("auth_address")
    val authAddress: String

    @JsName("cert_sign")
    val certSign: String
}

fun Cert(
        authUserName: String = "",
        authType: String = "",
        authPrivateKey: String = "",
        authAddress: String = "",
        certSign: String = "") =
        json(
            "auth_user_name" to authUserName,
            "auth_type" to authType,
            "auth_privatekey" to authPrivateKey,
            "auth_address" to authAddress,
            "cert_sign" to certSign)