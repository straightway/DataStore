package zeronet

import org.w3c.dom.events.Event
import straightway.utils.AsyncEvent
import straightway.utils.AsyncEventRegistry
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.js.json

class ZeroFrameImpl(private val context: ZeroFrameContext) : ZeroFrame {

    // region Private properties

    private val waitingCallbacks: MutableMap<Int, (Any?) -> Unit> = mutableMapOf()
    private val wrapperNonce = WRAPPER_REGEX.replace(context.browser.document.location!!.href) { it.groupValues[1] }
    private val openWebSockedEvent = AsyncEvent<Unit>()
    private val closeWebSockedEvent = AsyncEvent<Unit>()
    private val setSiteInfoEvent = AsyncEvent<SiteInfoEvent>()
    private val requestEvent = AsyncEvent<Message>()

    // endregion

    // region ZeroFrame

    override suspend fun <T> cmd(cmd: String, params: Any?): T =
            suspendCoroutine { c -> cmdImpl(cmd, params.normalized) { it: T -> c.resume(it) } }

    override fun fireAndForget(cmd: String, params: Any?) {
        context.target.postMessage(message(cmd = cmd, params = params.normalized))
    }

    override val openWebSocket: AsyncEventRegistry<Unit> = openWebSockedEvent
    override val closeWebsocket: AsyncEventRegistry<Unit> = closeWebSockedEvent
    override val setSiteInfo: AsyncEventRegistry<SiteInfoEvent> = setSiteInfoEvent
    override val request: AsyncEventRegistry<Message> = requestEvent
    override val logger = context.logger

    // endregion

    // region Private

    private companion object {
        val WRAPPER_REGEX = Regex(".*wrapper_nonce=([A-Za-z0-9]+).*")
        const val CMD_INNER_READY = "innerReady"
        const val CMD_PONG = "pong"
    }

    init {
        context.launcher.launch {
            context.target.addEventListener("message") { context.launcher.launch { onMessage(it) } }
            cmd<Unit>(CMD_INNER_READY)
        }
    }

    @Suppress("UNCHECKED_CAST")
    private val Any?.normalized get() = when (this) {
        is Pair<*, *> -> json(this as Pair<String, Any?>)
        else -> this
    }

    private suspend fun onMessage(e: Event) {
        @Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
        val message = ((e as MessageEvent)).data

        @Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
        when (message.cmd) {
            "response" -> handleResponse(message)
            "wrapperReady" -> context.target.postMessage(message(cmd = CMD_INNER_READY))
            "ping" -> respond(message.id, CMD_PONG)
            "wrapperOpenedWebsocket" -> openWebSockedEvent(Unit)
            "wrapperClosedWebsocket" -> closeWebSockedEvent(Unit)
            "setSiteInfo" -> {
                logger.trace("ZeroFrame", "Received site info: ${JSON.stringify(message.params)}")
                setSiteInfoEvent(message.params as SiteInfoEvent)
            }
            else -> requestEvent(message)
        }
    }

    private fun respond(to: Int, result: Any?) =
            context.target.postMessage(message(cmd = "response", to = to, result = result))

    private fun handleResponse(message: Message) {
        logger.trace("ZeroFrame", "Response: ${JSON.stringify(message)}")
        val callback = waitingCallbacks[message.to]
        if (callback != null) {
            callback(message.result)
            waitingCallbacks.remove(message.to)
        }
    }

    private fun <T> cmdImpl(cmd: String, params: Any?, cb: (T) -> Unit) =
        message(cmd = cmd, params = params).also { msg ->
            context.target.postMessage(msg)
            @Suppress("UNCHECKED_CAST")
            waitingCallbacks[msg.id] = { it: Any? ->cb(it as T) }
        }

    private fun message(cmd: String, to: Int = 0, params: Any? = null, result: Any? = null) =
            Message(cmd, to = to, params = params, result = result, wrapperNonce = wrapperNonce).apply {
                logger.trace("ZeroFrame", "New Message: ${JSON.stringify(this)}")
            }

    // endregion
}