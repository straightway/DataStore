package zeronet

external interface MessageEvent {
    val data: Message
}