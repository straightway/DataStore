package zeronet

external interface SiteCache {
    @JsName("time_modified_files_check")
    val timeModifiedFilesCheck: Double

    @JsName("modified_files")
    val modifiedFiles: Array<String>
}