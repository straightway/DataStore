package zeronet

import kotlin.js.json

interface Db {
    suspend fun <T> query(query: String, vararg params: Pair<String, Any?>): Array<T>
}

val ZeroFrame.db get() = object : Db {
    override suspend fun <T> query(query: String, vararg params: Pair<String, Any?>) =
        cmd<Array<T>>("dbQuery", arrayOf(query, json(*params)))
}