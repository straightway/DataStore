package zeronet

import kotlin.js.Date

interface Logger {

    enum class Level(val abbreviation: String, val index: Int) {
        Trace("T", -1),
        Debug("D", 0),
        Info("I", 1),
        Warning("W", 2),
        Error("E", 3),
        Fatal("F", 4)
    }

    fun log(l: Level, component: String, s: String)

    companion object {
        operator fun invoke(
                minLevel: Level = Level.Warning,
                componentFilter: Regex? = null
        ) = object : Logger {
            override fun log(l: Level, component: String, s: String) {
                if (l.index < minLevel.index) return
                if (componentFilter != null && !componentFilter.containsMatchIn(component)) return
                val msg = "${Date(Date.now()).toISOString()} ${l.abbreviation} [$component]: $s"
                when (l) {
                    Level.Trace, Level.Debug -> console.log(msg)
                    Level.Info -> console.info(msg)
                    Level.Warning -> console.warn(msg)
                    Level.Error, Level.Fatal -> {
                        console.error(msg)
                        console.asDynamic()?.trace("Stack trace:")
                    }
                }
            }
        }
    }
}

fun Logger.trace(component: String, s: String) = log(Logger.Level.Trace, component, s)
fun Logger.debug(component: String, s: String) = log(Logger.Level.Debug, component, s)
fun Logger.info(component: String, s: String) = log(Logger.Level.Info, component, s)
fun Logger.warn(component: String, s: String) = log(Logger.Level.Warning, component, s)
fun Logger.error(component: String, s: String) = log(Logger.Level.Error, component, s)
fun Logger.fatal(component: String, s: String) = log(Logger.Level.Fatal, component, s)