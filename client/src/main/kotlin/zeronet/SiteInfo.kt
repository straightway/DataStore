package zeronet

import kotlin.js.json

external interface SiteInfo {
    @JsName("auth_key")
    val authKey: String

    @JsName("auth_address")
    val authAddress: String

    @JsName("cert_user_id")
    val certUserId: String

    @JsName("address")
    val address: String

    @JsName("address_short")
    val addressShort: String

    @JsName("settings")
    val settings: SiteSettings

    @JsName("content_updated")
    val contentUpdated: Boolean

    @JsName("bad_files")
    val badFiles: Int

    @JsName("size_limit")
    val sizeLimit: Int

    @JsName("next_size_limit")
    val nextSizeLimit: Int

    @JsName("peers")
    val peers: Int

    @JsName("started_task_num")
    val startedTaskNum: Int

    @JsName("tasks")
    val tasks: Int

    @JsName("workers")
    val workers: Int

    @JsName("content")
    val content: SiteContent

    @JsName("privatekey")
    val privateKey: Boolean

    @JsName("feed_follow_num")
    val feedFollowNum: Int?
}

fun SiteInfo(
        authKey: String = "",
        authAddress: String = "",
        certUserId: String = "",
        address: String = "",
        addressShort: String = "",
        settings: SiteSettings = SiteSettings(),
        contentUpdated: Boolean = false,
        badFiles: Int = 0,
        sizeLimit: Int = 0,
        nextSizeLimit: Int = 0,
        peers: Int = 0,
        startedTaskNum: Int = 0,
        tasks: Int = 0,
        workers: Int = 0,
        content: SiteContent = SiteContent(),
        privateKey: Boolean = false,
        feedFollowNum: Int? = null): SiteInfo =
        json(
                "auth_key" to authKey,
                "auth_address" to authAddress,
                "cert_user_id" to certUserId,
                "address" to address,
                "address_short" to addressShort,
                "settings" to settings,
                "content_updated" to contentUpdated,
                "bad_files" to badFiles,
                "size_limit" to sizeLimit,
                "next_size_limit" to nextSizeLimit,
                "peers" to peers,
                "started_task_num" to startedTaskNum,
                "tasks" to tasks,
                "workers" to workers,
                "content" to content,
                "privatekey" to privateKey,
                "feed_follow_num" to feedFollowNum).asDynamic()