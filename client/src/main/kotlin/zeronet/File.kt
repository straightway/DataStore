package zeronet

interface File {

    enum class Format(val str: String) {
        Base64("base64"),
        Text("text")
    }

    suspend fun read(
            innerPath: String,
            isRequired: Boolean = true,
            format: Format = Format.Text,
            timeoutMS: Int = 300): String
    suspend fun write(innerPath: String, contentBase64: String)
    suspend fun delete(innerPath: String)
    suspend fun need(innerPath: String, timeoutS: Int = 300)
    suspend fun <T> query(dirInnerPath: String, query: String? = null): Array<T>
    suspend fun list(dirInnerPath: String): Array<String>
}

val ZeroFrame.file get() = object : File {
    override suspend fun read(innerPath: String, isRequired: Boolean, format: File.Format, timeoutMS: Int) =
            cmd<String>(
                    "fileGet",
                    "inner_path" to innerPath,
                    "required" to isRequired,
                    "format" to format.str,
                    "timeout" to timeoutMS)

    override suspend fun write(innerPath: String, contentBase64: String) {
        cmdThrow("fileWrite", innerPath, contentBase64)
    }

    override suspend fun delete(innerPath: String) {
        cmdThrow("fileDelete", arrayOf(innerPath))
    }

    override suspend fun need(innerPath: String, timeoutS: Int) {
        cmdThrow("fileNeed", arrayOf(innerPath, timeoutS))
    }

    override suspend fun <T> query(dirInnerPath: String, query: String?): Array<T> =
            if (query == null)
                cmd("fileQuery", arrayOf(dirInnerPath))
            else cmd("fileQuery", arrayOf(dirInnerPath, query))

    override suspend fun list(dirInnerPath: String): Array<String> =
            cmd("fileList", arrayOf(dirInnerPath))
}