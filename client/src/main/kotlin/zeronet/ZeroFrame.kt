package zeronet

import org.w3c.xhr.XMLHttpRequest
import straightway.error.Panic
import straightway.utils.AsyncEventRegistry
import kotlin.browser.window
import kotlin.js.json

interface ZeroFrame {
    val openWebSocket: AsyncEventRegistry<Unit>
    val closeWebsocket: AsyncEventRegistry<Unit>
    val setSiteInfo: AsyncEventRegistry<SiteInfoEvent>
    val request: AsyncEventRegistry<Message>
    val logger: Logger

    suspend fun <T> cmd(cmd: String, params: Any? = null): T
    fun fireAndForget(cmd: String, params: Any? = null)

    companion object {
        operator fun invoke(context: ZeroFrameContext): ZeroFrame = ZeroFrameImpl(context)
    }
}

fun ZeroFrame.fireAndForget(cmd: String, vararg params: Pair<String, Any>) = fireAndForget(cmd, json(*params))

fun ZeroFrame.fireAndForget(cmd: String, vararg params: Any?) = fireAndForget(cmd, params)

suspend fun <T> ZeroFrame.cmd(cmd: String, vararg params: Pair<String, Any>): T = cmd<T>(cmd, json(*params))

suspend fun <T> ZeroFrame.cmd(cmd: String, vararg params: Any?): T = cmd<T>(cmd, params)

suspend fun ZeroFrame.cmdThrow(cmd: String, vararg params: Pair<String, Any>) {
    cmd<Any?>(cmd, *params).also { if (it != "ok") throw Panic(it ?: "null") }
}

suspend fun ZeroFrame.cmdThrow(cmd: String, vararg params: Any?) {
    cmd<Any?>(cmd, *params).also { if (it != "ok") throw Panic(it ?: "null") }
}

suspend fun ZeroFrame.cmdThrow(cmd: String, params: Any? = null) {
    cmd<Any?>(cmd, params).also { if (it != "ok") throw Panic(it ?: "null") }
}

suspend fun ZeroFrame.monkeyPatchAjax() {
    val page = asDynamic()

    val xmlHttpRequestPrototype = js("XMLHttpRequest.prototype")
    xmlHttpRequestPrototype.realOpen = XMLHttpRequest.asDynamic().prototype.open
    val newOpen: dynamic.(dynamic, String, dynamic) -> dynamic = { method: dynamic, url: String , async: dynamic ->
        this.realOpen(method, url + "?ajax_key=${page.ajax_key}", async)
    }
    xmlHttpRequestPrototype.prototype.open = newOpen

    window.asDynamic().realFetch = window.asDynamic().fetch
    val newFetch = { url: String ->
        window.asDynamic().realFetch(url + "?ajax_key=${page.ajax_key}")
    }
    window.asDynamic().fetch = newFetch

    page.ajax_key = cmd<String>("wrapperGetAjaxKey", arrayOf<Any?>())
}

inline suspend fun ZeroFrame.doNotifyingExceptionsAsync(block: () -> Unit) {
    try {
        block()
    } catch (x: Exception) {
        val msg = "Unexpected exception: ${JSON.stringify(x.message)}"
        logger.error("ZeroFrame", msg)
        val callStack = x.asDynamic().stack?.unsafeCast<String>()
        if (callStack != null) {
            logger.debug("ZeroFrame", "Call stack:")
            callStack.lines().forEach {
                logger.debug("ZeroFrame", "  $it")
            }
        }

        interaction.notifyError(msg)
    }
}