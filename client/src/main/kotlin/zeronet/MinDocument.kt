package zeronet

import org.w3c.dom.Location

external interface MinDocument {
    val location: Location?
}