package zeronet

external interface SiteInfoEvent : SiteInfo {
    @JsName("event")
    val event: Array<String>?
}