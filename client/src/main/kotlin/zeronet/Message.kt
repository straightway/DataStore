package zeronet

import kotlin.js.json

external interface Message {
    val cmd: String
    val params: Any?
    val to: Int
    val result: Any?
    val id: Int
    @JsName("wrapper_nonce")
    val wrapperNonce: String
}

private var nextMessageId = 1

@Suppress("UNCHECKED_CAST_TO_EXTERNAL_INTERFACE")
fun Message(cmd: String, to: Int = 0, params: Any? = null, result: Any? = null, wrapperNonce: String = "") =
        json(
                "cmd" to cmd,
                "to" to to,
                "params" to params,
                "result" to result,
                "id" to nextMessageId++,
                "wrapper_nonce" to wrapperNonce) as Message
