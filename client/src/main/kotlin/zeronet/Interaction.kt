package zeronet

interface Interaction {
    enum class NotificationType(val str: String) {
        Error("error"),
        Info("info"),
        Done("done")
    }

    fun notify(type: NotificationType, msg: String, timeoutMS: Int = -1)

    suspend fun certSelect(
            vararg acceptedDomains: String,
            acceptAny: Boolean = false,
            acceptedPattern: String? = null)

}

fun Interaction.notifyInfo(msg: String, timeoutMS: Int = -1) =
        notify(Interaction.NotificationType.Info, msg, timeoutMS)

fun Interaction.notifyError(msg: String, timeoutMS: Int = -1) =
        notify(Interaction.NotificationType.Error, msg, timeoutMS)

fun Interaction.notifyDone(msg: String, timeoutMS: Int = -1) =
        notify(Interaction.NotificationType.Done, msg, timeoutMS)

val ZeroFrame.interaction get() = object : Interaction {
    override fun notify(type: Interaction.NotificationType, msg: String, timeoutMS: Int): Unit=
            if (0 <= timeoutMS) fireAndForget("wrapperNotification", type.str, msg, timeoutMS)
            else fireAndForget("wrapperNotification", type.str, msg)

    override suspend fun certSelect(
            vararg acceptedDomains: String,
            acceptAny: Boolean,
            acceptedPattern: String?
    ) {
        cmd<Unit>(
                "certSelect",
                *notNullNamedParameters(
                        "accepted_domains" to acceptedDomains,
                        "accept_any" to acceptAny,
                        "accepted_pattern" to acceptedPattern
                ))
    }
}

