package zeronet

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

interface Launcher {
    fun launch(block: suspend () -> Unit)
    companion object {
        operator fun invoke() = object : Launcher {
            override fun launch(block: suspend () -> Unit) {
                GlobalScope.launch { block() }
            }
        }
    }
}