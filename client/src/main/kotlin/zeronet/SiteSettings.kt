package zeronet

import kotlin.js.json

external interface SiteSettings {

    @JsName("added")
    val added: Int

    @JsName("ajax_key")
    val ajaxKey: String

    @JsName("bytes_recv")
    val bytesRecv: Int

    @JsName("bytes_sent")
    val bytesSent: Int

    @JsName("cache")
    val cache: SiteCache

    @JsName("downloaded")
    val downloaded: Int

    @JsName("modified")
    val modified: Int

    @JsName("optional_downloaded")
    val optionalDownloaded: Int

    @JsName("own")
    val own: Boolean

    @JsName("peers")
    val peers: Int

    @JsName("permissions")
    val permissions: Array<String>

    @JsName("serving")
    val serving: Boolean

    @JsName("size")
    val size: Int

    @JsName("size_files_optional")
    val sizeFilesOptional: Int

    @JsName("size_limit")
    val sizeLimit: Int

    @JsName("size_optional")
    val sizeOptional: Int
}

fun SiteSettings(): SiteSettings = json().asDynamic()