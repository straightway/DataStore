package zeronet

interface Plugin {
    suspend fun addRequest(pluginPath: String)
}

val ZeroFrame.plugin get() = object : Plugin {
    override suspend fun addRequest(pluginPath: String) {
        cmdThrow("pluginAddRequest", pluginPath)
    }
}