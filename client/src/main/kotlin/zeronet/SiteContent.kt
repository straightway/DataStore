package zeronet

import kotlin.js.json

external interface SiteContent {

    @JsName("address")
    val address: String

    @JsName("address_index")
    val addressIndex: Int

    @JsName("clone_root")
    val cloneRoot: String

    @JsName("cloned_from")
    val clonedFrom: String

    @JsName("description")
    val description: String

    @JsName("files")
    val files: Int

    @JsName("ignore")
    val ignore: String

    @JsName("includes")
    val includes: Int

    @JsName("inner_path")
    val innerPath: String

    @JsName("modified")
    val modified: Int

    @JsName("postmessage_nonce_security")
    val postmessageNonceSecurity: Boolean

    @JsName("signs_required")
    val signsRequired: Boolean

    @JsName("title")
    val title: String

    @JsName("zeronet_version")
    val zeronetVersion: String

    @JsName("files_optional")
    val filesOptional: Int
}

val SiteContent.backgroundColor: String get() = asDynamic()["background-color"]

fun SiteContent(): SiteContent = json().asDynamic()