package zeronet

interface Site {
    suspend fun getInfo(): SiteInfo
    suspend fun getServerInfo(): ServerInfo
    suspend fun sign(privateKey: String? = null, innerPath: String? = null, removeMissionOptions: Boolean = false)
    suspend fun publish(privateKey: String? = null, innerPath: String? = null, sign: Boolean = true)
}

val ZeroFrame.site get() = object : Site {

    override suspend fun getInfo(): SiteInfo = cmd("siteInfo")

    override suspend fun getServerInfo(): ServerInfo = cmd("serverInfo")

    override suspend fun sign(privateKey: String?, innerPath: String?, removeMissionOptions: Boolean) {
        cmdThrow(
                "siteSign",
                *notNullNamedParameters(
                        "remove_missing_optional" to removeMissionOptions,
                        "privatekey" to privateKey,
                        "inner_path" to innerPath))
    }

    override suspend fun publish(privateKey: String?, innerPath: String?, sign: Boolean) {
        cmdThrow(
                "sitePublish",
                *notNullNamedParameters(
                        "sign" to sign,
                        "privatekey" to privateKey,
                        "inner_path" to innerPath
                ))
    }
}