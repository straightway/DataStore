package zeronet

import kotlin.js.json

external interface ServerPortOpened {
    @JsName("ipv4")
    val ipv4: Boolean

    @JsName("ipv6")
    val ipv6: Boolean
}

fun ServerPortOpened(ipv4: Boolean = false, ipv6: Boolean = false): ServerPortOpened =
        json(
                "ipv4" to ipv4,
                "ipv6" to ipv6).asDynamic()