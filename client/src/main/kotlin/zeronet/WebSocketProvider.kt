package zeronet

import kotlin.browser.window

interface WebSocketProvider {
    fun postMessage(message: Any?)
    fun addEventListener(type: String, callback: ((dynamic) -> Unit)?)

    companion object {
        operator fun invoke(): WebSocketProvider = object : WebSocketProvider
        {
            override fun postMessage(message: Any?)
                    = window.parent.postMessage(message, "*")
            override fun addEventListener(type: String, callback: ((dynamic) -> Unit)?)
                    = window.addEventListener(type, callback)
        }
    }
}