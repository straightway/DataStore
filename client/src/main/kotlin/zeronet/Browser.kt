package zeronet

interface Browser {
    val document: MinDocument

    companion object {
        operator fun invoke(): Browser = object : Browser {
            override val document: MinDocument
                get() = object : MinDocument {
                override val location get() = kotlin.browser.document.location
            }
        }
    }
}