package zeronet

import kotlin.js.Json
import kotlin.js.json

external interface ServerInfo {

    @JsName("ip_external")
    val ipExternal: Boolean

    @JsName("port_opened")
    val portOpened: ServerPortOpened

    @JsName("platform")
    val platform: String

    @JsName("fileserver_ip")
    val fileserverIp: String

    @JsName("fileserver_port")
    val fileServerPort: Int

    @JsName("tor_enabled")
    val torEnabled: Boolean

    @JsName("tor_status")
    val torStatus: String

    @JsName("tor_has_meek_bridges")
    val torHasMeekBridges: Boolean

    @JsName("tor_use_bridges")
    val torUseBridges: Boolean

    @JsName("ui_ip")
    val uiIp: String

    @JsName("ui_port")
    val uiPort: Int

    @JsName("version")
    val version: String

    @JsName("rev")
    val rev: Int

    @JsName("timecorrection")
    val timeCorrection: Double

    @JsName("language")
    val language: String

    @JsName("debug")
    val debug: Boolean

    @JsName("offline")
    val offline: Boolean

    @JsName("plugins")
    val plugins: Array<String>

    @JsName("plugins_rev")
    val pluginsRev: Json

    @JsName("user_settings")
    val userSettings: Json
}

fun ServerInfo(
        ipExternal: Boolean = false,
        portOpened: ServerPortOpened = ServerPortOpened(),
        platform: String = "",
        fileserverIp: String = "",
        fileServerPort: Int = -1,
        torEnabled: Boolean = false,
        torStatus: String = "",
        torHasMeekBridges: Boolean = false,
        torUseBridges: Boolean = false,
        uiIp: String = "",
        uiPort: Int = -1,
        version: String = "",
        rev: Int = -1,
        timeCorrection: Double = 0.0,
        language: String = "",
        debug: Boolean = false,
        offline: Boolean = true,
        plugins: Array<String> = arrayOf(),
        pluginsRev: Json = json(),
        userSettings: Json = json()
) =
        json(
            "ip_external" to ipExternal,
            "port_opened" to portOpened,
            "platform" to platform,
            "fileserver_ip" to fileserverIp,
            "fileserver_port" to fileServerPort,
            "tor_enabled" to torEnabled,
            "tor_status" to torStatus,
            "tor_has_meek_bridges" to torHasMeekBridges,
            "tor_use_bridges" to torUseBridges,
            "ui_ip" to uiIp,
            "ui_port" to uiPort,
            "version" to version,
            "rev" to rev,
            "timecorrection" to timeCorrection,
            "language" to language,
            "debug" to debug,
            "offline" to offline,
            "plugins" to plugins,
            "plugins_rev" to pluginsRev,
            "user_settings" to userSettings).asDynamic()