import java.util.Date
import java.text.SimpleDateFormat

plugins {
    id("org.jetbrains.kotlin.js") version "1.3.70"
    id("com.moowork.node") version "1.3.1"
    idea
}

group = ""
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(kotlin("stdlib-js"))
    implementation(kotlinx("coroutines-core-js", "1.3.3"))
    testImplementation(kotlin("test-js"))
}

kotlin.target.browser {
    testTask {
        useMocha {
        }
    }
}

fun kotlinx(module: String, version: String? = null): Any =
        "org.jetbrains.kotlinx:kotlinx-$module${version?.let { ":$version" } ?: ""}"

tasks {
    val createVersionInfo = register("createVersionInfo") {
        doLast {
            val dateFormat = SimpleDateFormat("yyyyMMddHHmmss")
            val now = dateFormat.format(Date())
            File(projectDir, "src/main/kotlin/straightway/datastore/version.kt")
                    .writeText("package straightway.datastore\nval VERSION = \"$now\"")
        }
    }

    "compileKotlinJs" { dependsOn(createVersionInfo) }
}